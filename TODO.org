 #+TITLE: Todo list for graph grammar
 #+author: Abdelrahman Madkour

* GUI
** Client [64%]
*** DONE Display bar
CLOSED: [2020-12-19 Sat 23:46]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-19 Sat 23:46]
- State "TODO"       from              [2020-12-16 Wed 12:02]
:END:
*** DONE Redo the things into functional components
CLOSED: [2020-12-16 Wed 20:59]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-16 Wed 20:59]
- State "TODO"       from              [2020-12-16 Wed 14:57]
:END:
*** DONE Come up with the mock ups of the components [100%]
CLOSED: [2020-12-19 Sat 15:48]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-19 Sat 15:48]
:END:
    - [X] Grammar View
      A view for the whole grammar
    - [X] Rule List View
      A view for the list of rules
    - [X] Grammar list View
    - [X] Graph Edit
      A view for editing a graph
    - [X] Axiom Edit
      A view for editing the inital graph
    - [X] Rule Edit
      A view for editing a rule
    - [X] Samples View
      A view for displaying samples from the grammar
    - [X] Scoring view
      A view for scoring samples from the grammar
*** DONE Make sure the graph components works with vis
CLOSED: [2020-12-14 Mon 13:24]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-14 Mon 13:24]
- State "TODO"       from              [2020-12-12 Sat 16:28]
:END:
*** DONE Make navbar prettier
CLOSED: [2020-12-20 Sun 13:43]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-20 Sun 13:43]
- State "TODO"       from              [2020-12-19 Sat 23:46]
:END:
*** DONE Redo props of graph component
CLOSED: [2020-12-18 Fri 11:01]
- [ ] Information to retrieve the graph.
- [ ] Information to make the graph editable or not
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-18 Fri 11:01]
- State "TODO"       from              [2020-12-15 Tue 15:53]
:END:
*** DONE Hook the components together
CLOSED: [2020-12-20 Sun 21:46]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-20 Sun 21:46]
- State "TODO"       from              [2020-12-12 Sat 16:28]
:END:
*** DONE Finish the axiom specifying component [0%]
CLOSED: [2020-12-20 Sun 23:50]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-20 Sun 23:50]
:END:
- [ ] Center the graph component
- [ ]  Make the graph component resize according to user browser
*** DONE prevent the same name of rule tol be specified
CLOSED: [2020-12-23 Wed 10:11]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-23 Wed 10:11]
- State "TODO"       from              [2020-12-20 Sun 19:54]
:END:
*** DONE Fix bug where adding a node without picking a button makes an empty node
CLOSED: [2020-12-23 Wed 10:11]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-23 Wed 10:11]
- State "TODO"       from              [2020-12-20 Sun 19:54]
:END:
*** DONE Finish the grammar screen
CLOSED: [2020-12-26 Sat 18:35]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-26 Sat 18:35]
- State "TODO"       from              [2020-12-15 Tue 15:59]
:END:
*** DONE Finish the alphabet drawer
CLOSED: [2020-12-26 Sat 18:35]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-26 Sat 18:35]
- State "TODO"       from              [2020-12-19 Sat 15:53]
:END:
**** DONE move the alphabet drawer to components that use it
CLOSED: [2020-12-20 Sun 13:43]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-20 Sun 13:43]
- State "TODO"       from              [2020-12-19 Sat 16:05]
:END:
**** DONE Change the icons into svgs
CLOSED: [2020-12-26 Sat 18:35]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-26 Sat 18:35]
- State "TODO"       from              [2020-12-20 Sun 23:16]
:END:

*** DONE Figure out how to resize the grid according to screen size not raw pixels
CLOSED: [2020-12-26 Sat 18:36]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-26 Sat 18:36]
- State "TODO"       from              [2020-12-16 Wed 23:03]
:END:
*** DONE Finish the rule specifying component [100%]
CLOSED: [2021-01-04 Mon 15:37]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-01-04 Mon 15:37]
:END:
- [X] Make the graph component resize according to user browser
- [X] Add the min max ranges of the rule
- [X] add the listing of other rhs
- [X] allow the creation of new rhs
**** DONE change  buttons to floating action buttons
CLOSED: [2020-12-26 Sat 18:36]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-26 Sat 18:36]
:END:
**** DONE Add divders between rules in the rule listings for one rule. i.e.e when it has multiple rhs
CLOSED: [2021-01-04 Mon 15:37]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-01-04 Mon 15:37]
- State "TODO"       from              [2020-12-15 Tue 15:18]
:END:
*** DONE Refactor graph stuff into a util file
CLOSED: [2021-01-04 Mon 17:26]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-01-04 Mon 17:26]
- State "TODO"       from              [2020-12-18 Fri 22:29]
:END:
*** DONE make a react context and store the grammar there
CLOSED: [2021-02-03 Wed 08:03]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-03 Wed 08:03]
- State "TODO"       from              [2020-12-21 Mon 02:29]
:END:
*** DONE Adding a grammar for now will use the "default" alphabet
CLOSED: [2021-02-03 Wed 08:04]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-03 Wed 08:04]
- State "TODO"       from              [2020-12-16 Wed 14:55]
:END:
*** DONE Figure out how to store stuff so that the grammar changes do not go away
CLOSED: [2021-02-05 Fri 19:35]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-05 Fri 19:35]
- State "TODO"       from              [2021-02-02 Tue 13:11]
:END:
*** DONE Add alphabet edit functionality
CLOSED: [2021-02-07 Sun 15:26]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-07 Sun 15:26]
- State "TODO"       from              [2020-12-15 Tue 15:56]
:END:
*** DONE Figure out svgs for the letters
CLOSED: [2021-02-07 Sun 15:26]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-07 Sun 15:26]
- State "TODO"       from              [2021-02-03 Wed 08:10]
:END:
*** DONE Delete rules
CLOSED: [2021-02-08 Mon 06:09]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-08 Mon 06:09]
- State "TODO"       from              [2021-02-07 Sun 21:09]
:END:
*** DONE Alphabet view in the grammar view
CLOSED: [2021-02-08 Mon 06:09]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-08 Mon 06:09]
- State "TODO"       from              [2021-02-06 Sat 09:51]
:END:
*** DONE Add option for RHS to be deleted
CLOSED: [2021-02-08 Mon 07:19]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-08 Mon 07:19]
- State "TODO"       from              [2021-02-07 Sun 22:01]
:END:
*** DONE Replace buttons on sample screen with toggle buttons
CLOSED: [2021-03-01 Mon 21:09]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-01 Mon 21:09]
- State "TODO"       from              [2021-02-22 Mon 12:35]
:END:
*** DONE Fix the loading of the rules
CLOSED: [2021-03-01 Mon 21:10]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-01 Mon 21:10]
- State "TODO"       from              [2021-02-07 Sun 15:25]
:END:
*** TODO Finish the sampling/scoring screen
:LOGBOOK:
- State "TODO"       from              [2020-12-19 Sat 15:53]
:END:
**** DONE Do the scoring gui interactions
CLOSED: [2021-02-03 Wed 08:05]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-03 Wed 08:05]
- State "TODO"       from              [2020-12-18 Fri 11:01]
:END:
**** DONE Add sliders to allow users to score the samples
CLOSED: [2021-02-03 Wed 08:05]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-03 Wed 08:05]
:END:
**** DONE Have option to make sample read only
CLOSED: [2021-02-03 Wed 08:05]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-03 Wed 08:05]
- State "TODO"       from              [2021-01-05 Tue 03:13]
:END:
**** DONE Add new preference functions
CLOSED: [2021-03-10 Wed 18:40]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-10 Wed 18:40]
- State "TODO"       from              [2021-02-09 Tue 08:42]
:END:
**** TODO Filters for different distributions
**** TODO View current PDF and complementary PDF
:LOGBOOK:
- State "TODO"       from              [2021-02-09 Tue 08:42]
:END:
*** TODO Add a way to mark the nodes in the rule screen
:LOGBOOK:
- State "TODO"       from              [2021-04-01 Thu 17:25]
:END:
*** TODO A way of deleting preference functions.
:LOGBOOK:
- State "TODO"       from              [2021-03-02 Tue 13:25]
:END:
*** TODO Highlight the selected node in the drawer
:LOGBOOK:
- State "TODO"       from              [2021-02-28 Sun 18:50]
:END:
*** TODO Add warning on deletion to confirm that the rule/letter/grammar is about to be deleted
:LOGBOOK:
- State "TODO"       from              [2021-02-07 Sun 21:58]
:END:
*** TODO Figure out the parameters of each rule and then do a gui for it
:LOGBOOK:
- State "TODO"       from              [2021-01-05 Tue 10:35]
:END:
**** TODO Rule probability
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:40]
:END:
**** TODO Conditional statements
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:40]
:END:
**** TODO Registers (a la ludoscope)
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:41]
:END:
*** TODO Allow for edges to be directional
*** TODO Check if the letters used in the graphs are in the selected alphabet
:LOGBOOK:
- State "TODO"       from              [2021-02-06 Sat 09:49]
:END:
*** TODO Change alphabet edit to have the name
:LOGBOOK:
- State "TODO"       from              [2021-02-07 Sun 20:21]
:END:
*** TODO Ability to change alphabet from the list of alphabets  for the grammar
:LOGBOOK:
- State "TODO"       from              [2021-02-07 Sun 20:21]
:END:
*** TODO Add option non-terminal nodes
*** TODO Display derivation tree
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:48]
:END:
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:48]
:END:
*** TODO Add option to add recipes(derivation rules)
:LOGBOOK:
- State "TODO"       from              [2021-03-16 Tue 12:15]
:END:
*** TODO Bugs
:LOGBOOK:
- State "TODO"       from              [2021-02-04 Thu 15:47]
:END:
**** TODO Breadcrumbs needs to figure out what it based on the uri instead of which components the user traverses.
:LOGBOOK:
- State "TODO"       from              [2021-02-04 Thu 15:47]
:END:
**** DONE RHS and LHS have the same graph?
CLOSED: [2021-02-08 Mon 06:09]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-02-08 Mon 06:09]
- State "TODO"       from              [2021-02-07 Sun 22:03]
:END:
** Server [60%]
*** DONE Come up with mongoose schema for basic data
CLOSED: [2020-12-14 Mon 23:45]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-14 Mon 23:45]
:END:
*** DONE Set up routes for REST calls [50%]
CLOSED: [2021-02-07 Sun 15:26]
:LOGBOOK:
- State "DONE"       from "DONE"       [2021-02-07 Sun 15:26]
- State "DONE"       from "DONE"       [2021-02-07 Sun 15:26]
- State "DONE"       from "DONE"       [2021-02-07 Sun 15:26]
- State "DONE"       from "TODO"       [2021-02-07 Sun 15:26]
- State "TODO"       from "DONE"       [2020-12-16 Wed 14:54]
:END:
- [X] Routes for retrieving grammars
- [X] Routes for updating grammars
- [ ] Routes for retrieving end result graphs
- [ ] Routes for scoring end result graphs

*** DONE Test the REST calls
CLOSED: [2020-12-16 Wed 12:01]
:LOGBOOK:
- State "DONE"       from "TODO"       [2020-12-16 Wed 12:01]
:END:
*** DONE Figure out how to call a python script from the server
CLOSED: [2021-03-02 Tue 17:53]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-02 Tue 17:53]
- State "TODO"       from              [2020-12-12 Sat 16:31]
:END:
*** DONE Preference Functions are part of the grammar schema
CLOSED: [2021-03-10 Wed 18:44]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-10 Wed 18:44]
- State "TODO"       from              [2021-02-22 Mon 12:22]
:END:
*** DONE Change the graphs schema to include score
CLOSED: [2021-03-10 Wed 18:45]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-10 Wed 18:45]
- State "TODO"       from              [2020-12-18 Fri 11:01]
:END:
*** TODO Change the graph model to include vertex property
:LOGBOOK:
- State "TODO"       from              [2021-06-05 Sat 19:55]
:END:
*** TODO Figure out how to make the python script run in the background and process sampling requests
:LOGBOOK:
- State "TODO"       from              [2021-03-02 Tue 17:26]
:END:
:LOGBOOK:
- State "TODO"       from              [2021-03-02 Tue 17:26]
:END:
*** TODO Split the heroku into two, the front end and the back end
:LOGBOOK:
- State "TODO"       from              [2021-02-23 Tue 00:14]
:END:
*** TODO Deploy the thing on heroku/docker
:LOGBOOK:
- State "TODO"       from              [2020-12-15 Tue 16:00]
:END:
* Graph Grammar [90%]
** DONE Forward sampling grammar
CLOSED: [2021-03-10 Wed 18:42]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-10 Wed 18:42]
:END:
** DONE Set rule probabilities
CLOSED: [2021-03-27 Sat 11:07]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-27 Sat 11:07]
- State "TODO"       from              [2021-03-10 Wed 18:42]
:END:
** DONE Explicit terminal and non-terminal nodes
CLOSED: [2021-03-27 Sat 16:30]
Do this by making a vertex property, where 'T' and 'NT' delineate stuff
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-03-27 Sat 16:30]
- State "TODO"       from              [2021-03-10 Wed 18:43]
:END:
** DONE Keep track of generation tree for each generated graph
CLOSED: [2021-04-01 Thu 18:28]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-04-01 Thu 18:28]
- State "TODO"       from              [2021-03-10 Wed 18:43]
:END:
*** DONE keep track of the sets of productions as you generate and put them in a hash map. This hashmap will track how many times that set happened in the tree. Making it easier for us to figure out later what sets are in the tree
CLOSED: [2021-04-01 Thu 18:28]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-04-01 Thu 18:28]
- State "TODO"       from              [2021-04-01 Thu 15:33]
:END:
** DONE Example Grammar
CLOSED: [2021-04-04 Sun 00:06]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-04-04 Sun 00:06]
- State "TODO"       from              [2021-03-31 Wed 20:06]
:END:
** DONE Implement a probability for you to figure out which subgraph to pick based on history of which was picked in previous derivations
CLOSED: [2021-04-04 Sun 00:06]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-04-04 Sun 00:06]
- State "TODO"       from              [2021-03-29 Mon 00:46]
:END:
** DONE Store the grammar
CLOSED: [2021-05-30 Sun 10:46]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-05-30 Sun 10:46]
- State "TODO"       from              [2021-04-01 Thu 15:34]
:END:
** DONE Store the graphs
CLOSED: [2021-05-30 Sun 10:46]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-05-30 Sun 10:46]
- State "TODO"       from              [2021-04-04 Sun 00:06]
:END:
** DONE Draw the gen tree
CLOSED: [2021-05-30 Sun 10:46]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-05-30 Sun 10:46]
- State "TODO"       from              [2021-04-01 Thu 18:28]
:END:
** DONE Figure out if we are doing relative access levels correctly
CLOSED: [2021-06-05 Sat 19:55]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-06-05 Sat 19:55]
- State "TODO"       from              [2021-05-30 Sun 10:46]
:END:
** TODO Allow for conditionals that are more than just what other surrounding graphs there are
:LOGBOOK:
- State "TODO"       from              [2021-05-30 Sun 10:46]
:END:
* GPR [0%]
** TODO Graph kernels Grakel
:LOGBOOK:
- State "TODO"       from              [2021-03-18 Thu 20:29]
:END:
** TODO Set up custom Graph kernel
:LOGBOOK:
- State "TODO"       from              [2021-04-03 Sat 23:59]
:END:
** TODO Set up GPR with graph kernel
:LOGBOOK:
- State "TODO"       from              [2021-04-03 Sat 23:59]
:END:
* Adjusting Grammar [100%]
** DONE Parameter Learning [100%]
CLOSED: [2021-04-03 Sat 20:03]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-04-03 Sat 20:03]
:END:
*** DONE Implement the histogram method
CLOSED: [2021-04-03 Sat 20:03]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-04-03 Sat 20:03]
- State "TODO"       from              [2021-03-18 Thu 20:37]
:END:
* Improvements
** TODO Give insight of rules.
:LOGBOOK:
- State "TODO"       from              [2021-02-08 Mon 10:45]
:END:
** TODO Ability to use edge properties
:LOGBOOK:
- State "TODO"       from              [2021-06-05 Sat 14:37]
:END:
** TODO Use Sphinx to document the code
:LOGBOOK:
- State "TODO"       from              [2021-03-24 Wed 07:40]
:END:
** TODO Allow the user to add alphabet from grammarList. They can name it in the alphabet edit
:LOGBOOK:
- State "TODO"       from              [2021-02-06 Sat 09:51]
:END:

** TODO Conditional statements on each rule
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:42]
:END:

** TODO Allow for registers, that store global values
:LOGBOOK:
- State "TODO"       from              [2021-03-10 Wed 18:42]
:END:

** TODO Come up with a genre-specific feature mapping
:LOGBOOK:
- State "TODO"       from              [2021-03-18 Thu 20:30]
:END:

** TODO Possibly conditionals
:LOGBOOK:
- State "TODO"       from              [2021-03-31 Wed 20:05]
:END:
* Paper
** DONE Come up with a more game relevant toy example
CLOSED: [2021-06-11 Fri 16:11]
:LOGBOOK:
- State "DONE"       from "TODO"       [2021-06-11 Fri 16:11]
:END:
** TODO Abstract
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 16:11]
:END:
** TODO Manual scoring
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 16:23]
:END:
** TODO Evaluation section
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 16:11]
:END:
*** TODO The expressive range of the grammar before adjustment
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:54]
:END:
*** TODO The expressive range of the grammar after adjustment with metrics
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:54]
:END:
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:54]
:END:

*** TODO The expressive range of the grammar after adjustment with scoring
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:54]
:END:
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:54]
:END:
*** TODO Percentage of graphs below the threshold before and after adjustment with metrics
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:56]
:END:

*** TODO Percentage of graphs below the threshold before and after adjustment with scoring
:LOGBOOK:
- State "TODO"       from              [2021-06-11 Fri 17:56]
:END:
