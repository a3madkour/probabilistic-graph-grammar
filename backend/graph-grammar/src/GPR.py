import math
import torch
import gpytorch
from matplotlib import pyplot as plt
from os import mkdir
from os.path import exists, isdir
import numpy as np


class ExactGPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, state_dict):
        super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
        ard_num_dims = None
        if state_dict:
            ard_num_dims = state_dict["covar_module.base_kernel.raw_lengthscale"].shape[
                1
            ]
        else:
            if len(train_x > 0):
                ard_num_dims = train_x[0].shape[0]
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(
            gpytorch.kernels.RBFKernel(ard_num_dims=ard_num_dims)
        )

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


class GPR(object):
    def __init__(self, grammar_id, preference_function):
        self._grammar_id = grammar_id
        self._preference_function = preference_function
        path_to_dir = "../data/models/" + self._grammar_id
        if not isdir(path_to_dir):
            mkdir(path_to_dir)
        self._path_to_model = path_to_dir + "/" + self._preference_function + ".pth"
        self._state_dict = None
        self._model = None
        # if exists(self._path_to_model):
        # self._state_dict = torch.load(self._path_to_model)
        # self._model,self._likelihood = self._initalizeModelLikelihood()

    @property
    def grammar_id(self):
        return self._grammar_id

    @grammar_id.setter
    def grammar_id(self, value):
        self._grammar_id = value

    @property
    def preference_function(self):
        return self._preference_function

    @preference_function.setter
    def preference_function(self, value):
        self._preference_function = value

    def _initalizeModelLikelihood(self, train_x=[], train_y=[]):
        # initialize likelihood and model
        likelihood = gpytorch.likelihoods.GaussianLikelihood()
        model = ExactGPModel(train_x, train_y, likelihood, self._state_dict)

        # load model if it exists
        if self._state_dict:
            model.load_state_dict(self._state_dict)

        return model, likelihood

    def trainGPR(self, sample, score):
        train_x = torch.tensor(sample).float()
        train_y = torch.tensor(score).float()

        model, likelihood = self._initalizeModelLikelihood(train_x, train_y)

        training_iter = 100
        # Find optimal model hyperparameters
        model.train()
        likelihood.train()

        # Use the adam optimizer
        optimizer = torch.optim.Adam(
            model.parameters(), lr=0.1
        )  # Includes GaussianLikelihood parameters

        # "Loss" for GPs - the marginal log likelihood
        mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)

        for i in range(training_iter):
            # Zero gradients from previous iteration
            optimizer.zero_grad()
            # Output from model
            output = model(train_x)
            # Calc loss and backprop gradients
            loss = -mll(output, train_y)
            loss.backward()
            # print('Iter %d/%d - Loss: %.3f   lengthscale: %.3f   noise: %.3f' % (
            #     i + 1, training_iter, loss.item(),
            #     model.covar_module.base_kernel.lengthscale.item(),
            #     model.likelihood.noise.item()
            # ))
            # print('Iter %d/%d - Loss: %.3f   noise: %.3f' % (
            #     i + 1, training_iter, loss.item(),
            #     model.likelihood.noise.item()
            # ))
            optimizer.step()

            # Get into evaluation (predictive posterior) mode
        model.eval()
        likelihood.eval()

        # Test points are regularly spaced along [0,1]
        # Make predictions by feeding model through likelihood
        # with torch.no_grad(), gpytorch.settings.fast_pred_var():
        #     test_x = torch.linspace(0, 1, 51)
        #     observed_pred = likelihood(model(test_x))

        self._state_dict = model.state_dict()
        self._model = model
        # with torch.no_grad():
        # save the model
        # torch.save(model.state_dict(),self._path_to_model)

    def predict(self, sample):
        if not self._state_dict:
            # model has not been trained yet
            print("model has not been trained yet")
            return (0, 0)

        with torch.no_grad(), gpytorch.settings.fast_pred_var():
            test_x = torch.tensor(sample).float()
            self._likelihood = gpytorch.likelihoods.GaussianLikelihood()
            observed_pred = self._likelihood(self._model(test_x))
            pred = observed_pred.mean
            variance = observed_pred.variance
            # cov = observed_pred.lazy_covariance_matrix.numpy()
            # return (pred.item(), cov[0][0].item())
            return pred, variance
