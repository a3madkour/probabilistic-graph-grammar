from os.path import isfile, join, isdir


class RecipeEntry(object):
    def __init__(self, name, min_ap, max_ap):
        self._name = name
        self._min_ap = min_ap
        self._max_ap = max_ap

    def __str__(self):
        return self._name + "," + str(self.min_ap) + "," + str(self.max_ap) + "\n"

    def __repr__(self):
        return self.__str__()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def min_ap(self):
        return self._min_ap

    @min_ap.setter
    def min_ap(self, value):
        self._min_ap = value

    @property
    def max_ap(self):
        return self._max_ap

    @max_ap.setter
    def max_ap(self, value):
        self._max_ap = value

    @staticmethod
    def read_file(filename):
        if not isfile(filename):
            print("filename does not exist")
            return []

        input_file = open(filename, "r")
        lines = input_file.readlines()
        recipe = []
        for line in lines:
            components = line.split(",")
            if len(components) < 1:
                continue
            name = components[0].strip()
            if len(components) < 3:
                min_ap = 1
                max_ap = 1
                recipe.append(RecipeEntry(name, min_ap, max_ap))
            else:
                min_ap = int(components[1])
                max_ap = int(components[2])
                recipe.append(RecipeEntry(name, min_ap, max_ap))

        return recipe


if __name__ == "__main__":
    recipe = RecipeEntry.read_file("khalifa-grammar/graphRecipe.txt")
    print(recipe)
