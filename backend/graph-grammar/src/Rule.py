class RHS(object):
    def __init__(self, graph, prob):
        """
        Constructor.
        Inputs:
            * graph - Graph object on the LHS.
            * abbrevs - Vertex labels
            * prob - Probablity
        Outputs: N/A
        """
        self._graph = graph
        self._prob = prob

    def __str__(self):

        return_str = str(self._graph) + ': ' + str(self._prob)
        return return_str

    def __repr__(self):
        return self.__str__()

    @property
    def graph(self):
        return self._graph

    @graph.setter
    def graph(self, value):
        self._graph = value

    @property
    def prob(self):
        return self._prob

    @prob.setter
    def prob(self, value):
        self._prob = value


class LHS(object):
    def __init__(self, graph):
        """
        Constructor.
        Inputs:
            * graph - Graph object on the LHS.
            * abbrevs - Vertex labels
            * prob - Probablity
        Outputs: N/A
        """
        self._graph = graph

    def __str__(self):

        return_str = str(self._graph)
        return return_str

    def __repr__(self):
        return self.__str__()

    @property
    def graph(self):
        return self._graph

    @graph.setter
    def graph(self, value):
        self._graph = value


class Rule(object):
    """
    Represents a rule consisting of a left-hand side and a list of right-hand
    sides and a probability.
    """

    def __init__(self, name, lhs, rhss, prob):
        """
        Constructor.
        Inputs:
            * lhs - Graph object on the LHS.
            * rhss - List of RHSs
            * prob - Probablity
        Outputs: N/A
        """
        self._name = name
        self._lhs = lhs
        self._rhss = rhss
        self._prob = prob

    def __str__(self):
        return self._name

    def __repr__(self):
        return self.__str__()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def lhs(self):
        return self._lhs

    @lhs.setter
    def lhs(self, value):
        self._lhs = value

    @property
    def rhss(self):
        return self._rhss

    @rhss.setter
    def rhss(self, value):
        self._rhss = value

    @property
    def prob(self):
        return self._prob

    @prob.setter
    def prob(self, value):
        self._prob = value

# if __name__ == '__main__':
#     print('Hi')
