#!/usr/bin/env python3
import json
import uuid
from os import listdir
from os.path import isfile, join
import graph_tool.all as gt

directory = "../data/sample-graphs/to-score/"

onlyfiles = [directory + f for f in listdir(directory) if isfile(join(directory, f))]


def convertGraph(graph, graph_name):
    graph_dict = {}
    id_map = {}
    graph_dict["graph_id"] = graph_name
    graph_dict["nodes"] = []
    for v in g.vertices():
        node = {}
        v_id = uuid.uuid4()
        id_map[v] = str(v_id)
        node["label"] = graph.vp.vertex_property[v].label
        node["abbrev"] = graph.vp.vertex_property[v].abbrev
        node["id"] = str(v_id)
        graph_dict["nodes"].append(node)

    graph_dict["edges"] = []
    for e in g.edges():
        u, v = e
        e_id = uuid.uuid4()
        edge = {"from": id_map[u], "to": id_map[v], "id": str(e_id)}
        graph_dict["edges"].append(edge)

    return graph_dict


graphs = {}
for i, fil in enumerate(onlyfiles):
    g = gt.load_graph(fil)
    graph_string = "graph_" + str(i)
    graphs[graph_string] = convertGraph(g, fil)


a_file = open("graphs.json", "w")
json.dump(graphs, a_file, indent=4)
a_file.close()
