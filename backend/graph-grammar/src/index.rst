.. Probabilistic Graph Grammars documentation master file, created by
   sphinx-quickstart on Mon Jun  7 20:00:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Probabilistic Graph Grammars's documentation!
========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
