#!/usr/bin/env python3
# TODO
# Figure out how to deal with ANY
# If there is a wild card just check with access level and then do a post check of the labels on each of the matches.
# OR just make a class that takes care of the checking for you and use that as the vertex map property
# Figre out how to deal with access level
# add access level as a property
# add a check if access level is a thing then make a new property that is a vector of string that contains both labels and acess level
# This will the checking will be on both access level and labels
# Figre out how to deal with disconnected graphs
# Check if there are no edges in the graph to be matched
# if there are no edges check the vertices and check if the big graph has the vertices of the subgraph
# Then check that those verticies are not connetected in the big graph
# If so then you have a match
#
#
# Access level is RELATIVE, so that is an issue...
# Keeps track of relative access level for each subgraph and adjusts the patterns access level acoringly
# consider addiding conditionals to the grammar rules. like if the nodes have a degree more than 4 e should stop
# add the ability to use recipes


from os import listdir, mkdir, remove
from os.path import isfile, join, isdir
import graph_tool.all as gt
import logging
import uuid
import copy
import json
import pickle
import networkx as nx
from GPR import GPR
from Rule import Rule, RHS, LHS
from karateclub import FeatherGraph, LDP

from Metrics import _dijkstra, MapLinearity, MissionLinearity
from Generator import Generator
from networkx.algorithms.graph_hashing import weisfeiler_lehman_graph_hash
from sklearn.metrics import mean_squared_error, r2_score
from VertexProperty import VertexProperty
from RecipeEntry import RecipeEntry
import heapq
import sys


def gtToNxGraph(gt_graph):
    graph = {"nodes": [], "edges": []}
    nx_G = nx.Graph()

    id_map = {}
    for v in gt_graph.vertices():
        v_id = uuid.uuid4()
        id_map[v] = str(v_id)
        node = {"label": gt_graph.vp.vertex_property[v].abbrev, "id": str(v_id)}
        graph["nodes"].append(node)
        nx_G.add_node(int(v))

    for e in gt_graph.edges():
        u, v = e
        edge = {"from": id_map[u], "to": id_map[v]}
        graph["edges"].append(edge)
        nx_G.add_edge(int(u), int(v), weight=1)

    return nx_G


def SaveTrainingSet(samples, grammar):
    dir_path = "../data/trees/training-set/"
    if not isdir(dir_path):
        mkdir(dir_path)
    for sample_id in samples:
        filename = "../data/generation-trees/" + grammar + "/" + sample_id + ".pkl"
        input_file = open(filename, "rb")
        tree = pickle.load(input_file)
        tree.saveTree(dir_path + sample_id + ".pkl")


def checkIfSolvable(graph, root, visted=None):
    queue = []
    locks = []
    if not visted:
        visted = set()
    visted.add(root)
    children = graph.get_out_neighbors(root)
    for c in children:
        queue.append(c)
    keys = 0
    while len(queue) > 0:
        current = queue.pop(0)
        current_vp = graph.vp.vertex_property[current]
        if current in visted:
            continue
        if current_vp.label == "Lock":
            locks.append(current)
        else:
            if current_vp.label == "End":
                return True
            elif current_vp.label == "Key":
                keys += 1
            visted.add(current)
            current_children = graph.get_out_neighbors(current)
            for ch in current_children:
                queue.append(ch)
    required_keys = 0
    for l in locks:
        new_visted = set()
        for v in visted:
            new_visted.add(v)
        if not checkIfSolvable(graph, l, new_visted):
            required_keys = required_keys + 1

    if required_keys < keys:
        return True

    return False


labels_to_abbrevs = {
    "Start": "S",
    "Any": "A",
    "Lock": "Lo",
    "Normal": "N",
    "Lever": "Le",
    "Key": "K",
    "Puzzle": "P",
    "End": "E",
}


# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


def makeGraph(lines):
    # g = gt.Graph(directed=False)
    g = gt.Graph()
    # g.vertex_properties["abbrevs"] = g.new_vertex_property("string")
    # g.vertex_properties["labels"] = g.new_vertex_property("string")
    # g.vertex_properties["terminality"] = g.new_vertex_property("bool")
    # g.vertex_properties["marks"] = g.new_vertex_property("string")
    # g.vertex_properties["access_level"] = g.new_vertex_property("string")
    g.vertex_properties["vertex_property"] = g.new_vertex_property("python::object")
    for line in lines:
        striped_line = line.strip()
        if striped_line == "nodes":
            is_node = True
            continue
        if striped_line == "edges":
            is_node = False
            continue
        components = striped_line.split(",")
        if is_node:
            label = components[2].strip()
            v = g.add_vertex()
            abbrev = labels_to_abbrevs[label]
            mark = components[3].strip()
            terminality = True
            if label == "Any":
                terminality = False
            g.vp.vertex_property[v] = VertexProperty(
                label, abbrev, terminality, mark, int(components[1])
            )
        else:
            u = components[0]
            v = components[1]
            g.add_edge(u, v)
    return g


directory = "../data/grammars/khalifa-grammar/"
onlydirs = [f for f in listdir(directory) if isdir(join(directory, f))]
rules = []
for folder in onlydirs:
    input_file_path = directory + folder + "/input.txt"
    input_file = open(input_file_path, "r")
    input_lines = input_file.readlines()
    output_file_path = directory + folder + "/output.txt"
    output_file = open(output_file_path, "r")
    output_lines = output_file.readlines()
    is_node = True
    lhs_graph = makeGraph(input_lines)
    lhs = LHS(lhs_graph)
    lhs_graph.vertex_properties["abbrevs"] = lhs_graph.new_vertex_property("string")
    for v in lhs_graph.vertices():
        lhs_graph.vp.abbrevs[v] = lhs_graph.vp.vertex_property[v].abbrev
    gt.graph_draw(
        lhs_graph,
        vertex_text=lhs_graph.vp.abbrevs,
        output="../data/imgs/" + folder + "-lhs.png",
    )
    rhs_graph = makeGraph(output_lines)
    rhs_graph.vertex_properties["abbrevs"] = rhs_graph.new_vertex_property("string")
    for v in lhs_graph.vertices():
        rhs_graph.vp.abbrevs[v] = rhs_graph.vp.vertex_property[v].abbrev
    gt.graph_draw(
        rhs_graph,
        vertex_text=rhs_graph.vp.abbrevs,
        output="../data/imgs/" + folder + "-rhs.png",
    )
    rhs = RHS(rhs_graph, 1)
    rule = Rule(folder, lhs, [rhs], 1)
    rules.append(rule)

axiom_path = directory + "graphStart.txt"
graph_file = open(axiom_path, "r")
axiom_lines = graph_file.readlines()
axiom = makeGraph(axiom_lines)

# print(axiom)

axiom.vertex_properties["abbrevs"] = axiom.new_vertex_property("string")
for v in axiom.vertices():
    axiom.vp.abbrevs[v] = axiom.vp.vertex_property[v].abbrev

gt.graph_draw(axiom, vertex_text=axiom.vp.abbrevs, output="axiom-before.png")

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
recipe = RecipeEntry.read_file(directory + "graphRecipe.txt")
gen = Generator(axiom, rules, "khalifa-grammar")
config = {"max_applications": 20, "sampling_method": "uniform"}
sample, sample_id = gen.applyRules(config)
# sample, sample_id = gen.applyRecipe(config, recipe)
sample.vertex_properties["abbrevs"] = sample.new_vertex_property("string")
for v in sample.vertices():
    sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev

gt.graph_draw(sample, vertex_text=sample.vp.abbrevs, output="testing-khalifa.png")
# _dijkstra(sample, 0, 1)
MapLinearity(sample)
MissionLinearity(sample, 0, 1)
# axiom.vertex_properties["abbrevs"] = axiom.new_vertex_property("string")
# for v in axiom.vertices():
#     axiom.vp.abbrevs[v] = axiom.vp.vertex_property[v].abbrev

# gt.graph_draw(axiom, vertex_text=axiom.vp.abbrevs, output="axiom-after-applying.png")


# training_graphs = []
# training_data_Y = []
# min_v = 100
# max_v = 0
# avg_leniency = 0
#
# hash_mapz = {}
# for i in range(10000):
#     recipe = RecipeEntry.read_file("khalifa-grammar/graphRecipe.txt")
#     gen = Generator(axiom, rules, "khalifa-grammar")
#     config = {"max_applications": 10, "sampling_method": "uniform"}
#     sample, sample_id = gen.applyRecipe(config, recipe)
#     # sample, sample_id = gen.applyRules(config)
#     isit = checkIfSolvable(sample, 0)
#     if not isit:
#         print("we found one boys let's blow this thing and go home")
#     nx_graph = gtToNxGraph(sample)
#     hash_key = weisfeiler_lehman_graph_hash(nx_graph)
#     if hash_key in hash_mapz:
#         hash_mapz[hash_key] = hash_mapz[hash_key] + 1
#     else:
#         hash_mapz[hash_key] = 1

# print(len(hash_mapz))
# a_file = open("testing-khalifa.json", "w")
# json.dump(hash_mapz, a_file, indent=4)
# a_file.close()
# sample.vertex_properties["abbrevs"] = sample.new_vertex_property("string")
# for v in sample.vertices():
#     sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev
# gt.graph_draw(
#     sample,
#     vertex_text=sample.vp.abbrevs,
#     output="testing" + str(i) + "-khalifa.png",
# )

#     leniency_v = leniency(sample)
#     if leniency_v > 0.6:
#         training_graphs.append(sample_id)
#     # nx_graph = gtToNxGraph(sample)
#     # training_graphs.append(nx_graph)
#     min_v = min(min_v, leniency_v)
#     max_v = max(max_v, leniency_v)

#     # if not isit:
#     #     print("found one")
#     # print()
#     avg_leniency = avg_leniency + leniency_v
#     training_data_Y.append(leniency(sample))
#     # dijkstra(sample, 0, 1)
# avg_leniency = avg_leniency / 100
# print("avg: ", avg_leniency)
# print(min_v, max_v)
# avg_leniency = 0

# SaveTrainingSet(training_graphs, "khalifa-grammar")
# for i in range(100):
#     axiom = makeGraph(axiom_lines)
#     gen = Generator(axiom, rules, "khalifa-grammar")
#     config = {"max_applications": 20, "sampling_method": "grammar"}
#     gen.learnParameters("../data/trees/training-set/")
#     sample, sample_id = gen.applyRecipe(config, recipe)
#     leniency_v = leniency(sample)
#     avg_leniency = avg_leniency + leniency_v
#     if leniency_v > 0.6:
#         training_graphs.append(sample_id)
#     min_v = min(min_v, leniency_v)
#     max_v = max(max_v, leniency_v)

# avg_leniency = avg_leniency / 100
# print("avg: ", avg_leniency)
# print(min_v, max_v)
# gpr = GPR("khalifa-grammar", "overall")
# ldp_graph = LDP()
# ldp_graph.fit(training_graphs)
# embed_train = ldp_graph.get_embedding()

# gpr.trainGPR(embed_train, training_data_Y)

# test_graphs = []
# test_data_Y = []
# for i in range(10):
#     axiom = makeGraph(axiom_lines)
#     recipe = RecipeEntry.read_file("khalifa-grammar/graphRecipe.txt")
#     gen = Generator(axiom, rules, "khalifa-grammar")
#     config = {"max_applications": 20, "sampling_method": "uniform"}
#     sample, sample_id = gen.applyRecipe(config, recipe)
#     isit = checkIfSolvable(sample, 0)
#     sample.vertex_properties["abbrevs"] = sample.new_vertex_property("string")
#     for v in sample.vertices():
#         sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev
#     gt.graph_draw(
#         sample,
#         vertex_text=sample.vp.abbrevs,
#         output="testing" + str(i) + "-khalifa.png",
#     )
#     nx_graph = gtToNxGraph(sample)
#     test_graphs.append(nx_graph)
#     # if not isit:
#     #     print("found one")
#     # print()
#     test_data_Y.append(leniency(sample))
#     # dijkstra(sample, 0, 1)

# ldp_graph = LDP()
# ldp_graph.fit(test_graphs)
# embed_test = ldp_graph.get_embedding()
# score, uncertainty = gpr.predict(embed_test)
# score = score.numpy().flatten()
# print("user score: ", test_data_Y)
# mse = mean_squared_error(test_data_Y, score)
# r2 = r2_score(test_data_Y, score)
# print("mse: ", mse)
# print("r2: ", r2)
# print("score: ", score)

# vertex_property_1 = VertexProperty("Start", 0)
# vertex_property_2 = VertexProperty("End", 0)

# vertex_property_3 = VertexProperty("Any", 0)
# vertex_property_4 = VertexProperty("Any", 0)

# vertex_property_5 = VertexProperty("Key", 1)
# vertex_property_6 = VertexProperty("Taban", 1)

# G = gt.Graph(directed=True)
# G.vertex_properties["vertex_property"] = G.new_vertex_property(
#     "python::object")
# G.vertex_properties["abbrevs"] = G.new_vertex_property("string")
# v = G.add_vertex()
# G.vp.vertex_property[v] = vertex_property_1
# G.vp.abbrevs[v] = "S"
# u = G.add_vertex()
# G.vp.vertex_property[u] = vertex_property_6
# G.vp.abbrevs[u] = "T"
# u = G.add_vertex()
# G.vp.vertex_property[u] = vertex_property_2
# G.vp.abbrevs[u] = "E"
# G.add_edge(0, 1)
# G.add_edge(1, 2)

# gt.graph_draw(G, vertex_text=G.vp.abbrevs, output="testG.png")

# G1 = gt.Graph(directed=True)
# G1.vertex_properties["vertex_property"] = G1.new_vertex_property(
#     "python::object")
# G1.vertex_properties["abbrevs"] = G1.new_vertex_property("string")
# v = G1.add_vertex()
# G1.vp.vertex_property[v] = vertex_property_3
# G1.vp.abbrevs[v] = "A"
# u = G1.add_vertex()
# G1.vp.abbrevs[u] = "A"
# G1.vp.vertex_property[u] = vertex_property_4
# G1.add_edge(u, v)

# gt.graph_draw(G1, vertex_text=G1.vp.abbrevs, output="testG1.png")


# listOfMatches = gt.subgraph_isomorphism(
#     G1,
#     G,
#     # vertex_label=(G1.vp.vertex_property, G.vp.vertex_property)
# )

# print(listOfMatches)

# solutions = []
# for match in listOfMatches:
#     itMatches = True
#     for u in G1.vertices():
#         v = match[u]
#         if G1.vp.vertex_property[u] != G.vp.vertex_property[v]:
#             itMatches = False
#             break
#     if itMatches:
#         solutions.append(match)

# for sol in solutions:
#     for u in G1.vertices():
#         v = sol[u]
#         print(G1.vp.vertex_property[u])
#         print(G.vp.vertex_property[v])
#         print("--------------------------")


# def noEdges(sub_graph, big_graph):
#     matches = {}
#     for u in sub_graph.vertices():
#         matches[u] = []
#         for v in big_graph.vertices():
#             prop1 = big_graph.vp.vertex_property[v]
#             prop2 = sub_graph.vp.vertex_property[u]
#             if prop1 == prop2:
#                 # print("houston we found a match")
#                 matches[u].append(v)

#     # print(matches)
#     # use get_all_edges with each of the us in matches
#     keys = list(matches.keys())
#     if(len(keys) > 2):
#         print("houston we have a problem")
#         return

#     candidates = []
#     for v1 in matches[keys[0]]:
#         neighbors = big_graph.get_all_neighbors(v1)
#         for v2 in matches[keys[0]]:
#             if v2 not in neighbors and v1 != v2:
#                 match = {}
#                 match[keys[0]] = v1
#                 match[keys[1]] = v2
#                 candidates.append(match)
#                 # print("houston we have a candidate: " +
#                 # "("+str(v1)+","+str(v2) + ")")

#     return candidates

# for u in matches:
# print(u)


# edgy = G1.get_edges()
# if len(edgy) < 1:
#     print("no edges")

# # Try the subgraph isomorphism again with no edges this time knowing how matches work
# listOfMatches = noEdges(G1, G)

# print(G1)
# print(G)
# for match in listOfMatches:
#     print("||||||||||||||||||||")
#     for ve in match:
#         prop1 = G1.vp.vertex_property[ve]
#         print("ve: ", ve, " match[ve]: ", match[ve])
#         prop2 = G.vp.vertex_property[match[ve]]
#         print(prop1)
#         print(prop2)
#         print(prop1 == prop2)
#         print("--------------------")
#     print("||||||||||||||||||||")
