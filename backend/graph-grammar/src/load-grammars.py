#!/usr/bin/env python3

import logging
from Generator import Generator
from VertexProperty import VertexProperty
import sys
from os.path import isfile, join, isdir
import graph_tool.all as gt
from Rule import Rule, RHS, LHS
from os import listdir, mkdir, remove
from networkx.algorithms.graph_hashing import weisfeiler_lehman_graph_hash
from bson import json_util
from bson.objectid import ObjectId
from Metrics import PathRedundancy
import argparse
from os import path
import json
import uuid
import networkx as nx
import pickle


def hasMoreThanFour(graph):
    for v in graph.vertices():
        if len(graph.get_all_neighbors(v)) > 4:
            print(graph.vp.vertex_property[v].abbrev)
            return True
    return False


def gtToNxGraph(gt_graph):
    graph = {"nodes": [], "edges": []}
    nx_G = nx.Graph()

    id_map = {}
    for v in gt_graph.vertices():
        v_id = uuid.uuid4()
        id_map[v] = str(v_id)
        node = {"label": gt_graph.vp.vertex_property[v].abbrev, "id": str(v_id)}
        graph["nodes"].append(node)
        nx_G.add_node(int(v))

    for e in gt_graph.edges():
        u, v = e
        edge = {"from": id_map[u], "to": id_map[v]}
        graph["edges"].append(edge)
        nx_G.add_edge(int(u), int(v), weight=1)

    return nx_G


def makeGraph(mongoGraph):
    g = gt.Graph()
    IDtoIndex = {}
    vertices = []
    g.vertex_properties["vertex_property"] = g.new_vertex_property("python::object")
    for i, node in enumerate(mongoGraph["nodes"]):
        v = g.add_vertex()
        IDtoIndex[node["id"]] = i
        abbrev = node["abbrev"]
        label = node["label"]
        terminality = False
        mark = node["mark"]

        vertices.append(v)
        g.vp.vertex_property[v] = VertexProperty(label, abbrev, terminality, mark)

    for edge in mongoGraph["edges"]:
        u = vertices[IDtoIndex[edge["from"]]]
        v = vertices[IDtoIndex[edge["to"]]]
        g.add_edge(u, v)
    return g


def makeGrammar(grammar):
    axiom = makeGraph(grammar["axiom"])
    # gt.graph_draw(axiom, vertex_text=abbrevs, output="img/axiom.png")

    rules = []
    for i, rule in enumerate(grammar["rules"]):
        rule_name = rule["name"]
        rule_lhs = rule["lhs"]
        lhs_graph = makeGraph(rule_lhs)
        lhs = LHS(lhs_graph)
        # gt.graph_draw(lhs_graph, vertex_text=lhs_abbrevs, output="lhs"+ str(i) +".png")
        rule_rhss = rule["rhs"]
        rhss = []
        for j, rule_rhs in enumerate(rule_rhss):
            rhs_graph = makeGraph(rule_rhs["graph"])
            rhs = RHS(rhs_graph, rule_rhs["probability"])
            rhss.append(rhs)
            # gt.graph_draw(
            #     rhs_graph, vertex_text=rhs_abbrevs, output="rhs-"  + str(i)+ "-" + str(j) + ".png"
            # )
        rules.append(Rule(rule_name, lhs, rhss, 1))

    return (axiom, rules)


with open("../data/grammars/dormans-grammar.json") as json_file:
    data = json.load(json_file)

axiom, rules = makeGrammar(data)
axiom.vertex_properties["abbrevs"] = axiom.new_vertex_property("string")
for v in axiom.vertices():
    axiom.vp.abbrevs[v] = axiom.vp.vertex_property[v].abbrev


def loadGraphs(grammar):
    directory = "../data/sample-graphs/" + grammar + "/"
    onlyfiles = [
        directory + f for f in listdir(directory) if isfile(join(directory, f))
    ]
    hash_mapz = {}
    for (i, filename) in enumerate(onlyfiles):
        G = gt.load_graph(filename)
        nx_graph = gtToNxGraph(G)
        hash_key = weisfeiler_lehman_graph_hash(nx_graph)
        gl = filename.split("/")
        cg = gl[4].split(".")
        sample_id = cg[0]
        hash_mapz[hash_key] = sample_id

    return hash_mapz


# print(hash_mapz)

with open("trees_location2.json") as json_file:
    data = json.load(json_file)

print(data)
directory = "../data/generation-trees/dormans-grammar/"
for tree in data:
    sample_id = data[tree]
    input_file = open(directory + sample_id + ".pkl", "rb")
    tree_s = pickle.load(input_file)
    tree_s.saveTree(tree + ".pkl")

# gt.graph_draw(axiom, vertex_text=axiom.vp.abbrevs, output="axiom-before-dormans.png")
config = {"max_applications": 50, "sampling_method": "uniform"}
gen = Generator(axiom, rules, "dormans-grammar")
# hash_mapz = {}
# sample, sample_id = gen.applyRules(config)
# tally = []
# non_critical_rooms = ["n", "l", "t"]
hash_mapz = loadGraphs("to-score")

trees_locations = {}
print(hash_mapz)


# for i in range(200000):
#     sample, sample_id = gen.applyRules(config)
#     nx_graph = gtToNxGraph(sample)
#     hash_key = weisfeiler_lehman_graph_hash(nx_graph)
#     if hash_key in hash_mapz:
#         trees_locations[hash_mapz[hash_key]] = sample_id
#         print("we found one!")
#         print("trees_location length: ", len(trees_locations))

#     if len(hash_mapz) == len(trees_locations):
#         break
# if hash_key in hash_mapz:
#     hash_mapz[hash_key] = hash_mapz[hash_key] + 1
# else:
#     hash_mapz[hash_key] = 1
# for v in sample.vertices():
#     sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev
# gt.graph_draw(
#     sample,
#     vertex_text=sample.vp.abbrevs,
#     output="dormans-sample" + str(i) + "-test.png",
# )
# for v in sample.vertices():
#     sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev
# gt.graph_draw(sample, vertex_text=sample.vp.abbrevs, output="dormans-sample-test.png")


# print(PathRedundancy(sample, non_critical_rooms))
# a_file = open("trees_location2.json", "w")
# json.dump(trees_locations, a_file, indent=4)
# a_file.close()

# TODO: Check how many unique graphs are generated
# if not grammar:
#     print("Grammar not found")
#     sys.exit()
