#!/usr/bin/env python3


import graph_tool.all as gt

g1 = gt.Graph(directed=False)

g1.vertex_properties["abbrevs"] = g1.new_vertex_property("string")

v = g1.add_vertex()
g1.vp.abbrevs[v] = "Normal"

v = g1.add_vertex()
g1.vp.abbrevs[v] = "Start"

g1.add_edge(0,1)





g2 = gt.Graph(directed=False)
g2.vertex_properties["abbrevs"] = g2.new_vertex_property("string")


v = g2.add_vertex()
g2.vp.abbrevs[v] = "Start"
v = g2.add_vertex()
g2.vp.abbrevs[v] = "Normal"





listOfMatches = gt.subgraph_isomorphism(
#define a new vertex map with vector<string> of both access level and vertex label
    g2,
    g1,
    vertex_label=(g2.vp.abbrevs,g1.vp.abbrevs),
    induced = True
)

# print(listOfMatches)

for match in listOfMatches:
    print("match")
    for ve in match:
        print(g2.vp.abbrevs[ve])
        print("ve: ", ve, " match[ve]: ", match[ve])
        print(g1.vp.abbrevs[match[ve]])
