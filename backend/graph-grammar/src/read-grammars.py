#!/usr/bin/env python3

import pymongo
import logging
from Generator import Generator
import sys
import graph_tool.all as gt
from Rule import Rule, RHS, LHS
import requests
import uuid
from datetime import datetime
from bson.objectid import ObjectId
from karateclub import FeatherGraph
import networkx as nx
import numpy as np
import random
from GPR import GPR
import argparse
from os import path
import json
from bson import json_util

client = pymongo.MongoClient("localhost", 27017)

db = client["graph-grammar"]

parser = argparse.ArgumentParser(
    description="Load the grammar and save it as JSON for now"
)

grammar = db["grammars"].find_one({"_id": ObjectId("60bb831a5615e67c688a2446")})


loaded_grammar = json.loads(json_util.dumps(grammar))

with open("dormans-grammar.json", "w") as fp:
    json.dump(loaded_grammar, fp, indent=4)

# if not grammar:
#     print("Grammar not found")
#     sys.exit()
