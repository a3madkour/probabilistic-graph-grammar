#!/usr/bin/env python3
import pymongo
import logging
from Generator import Generator
import sys
import graph_tool.all as gt
from Rule import Rule, RHS, LHS
import requests
import uuid
from datetime import datetime
from bson.objectid import ObjectId
from karateclub import FeatherGraph
import networkx as nx
import numpy as np
import random
from GPR import GPR
from os import path

# startTime = datetime.now()

# use applyProductiosn to run the forward sampling
# It needs startgraph production(list of productions) and config(dicts)

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
client = pymongo.MongoClient("localhost", 27017)

db = client["graph-grammar"]

grammar_id = ""
query_id = ""
preference_func = ""
# default values
sample_size = 10
sampling_method = "grammar"

sampling_methods = ["grammar", "uniform", "learnt_pdf", "bo", "uncertainty"]


# python has an args module man, use that instead
if len(sys.argv) == 3:
    query_id = sys.argv[1]
    grammar_id = sys.argv[2]
elif len(sys.argv) == 4:
    query_id = sys.argv[1]
    grammar_id = sys.argv[2]
    if sys.argv[3] in sampling_methods:
        sampling_method = sys.argv[3]
elif len(sys.argv) == 5:
    query_id = sys.argv[1]
    grammar_id = sys.argv[2]
    if sys.argv[3] in sampling_methods:
        sampling_method = sys.argv[3]
    try:
        sample_size = int(sys.argv[4])
    except:
        sample_size = 10
elif len(sys.argv) == 6:
    query_id = sys.argv[1]
    grammar_id = sys.argv[2]
    if sys.argv[3] in sampling_methods:
        sampling_method = sys.argv[3]
    try:
        sample_size = int(sys.argv[4])
    except:
        sample_size = 10
    preference_func = sys.argv[5]
# sys.exit()
# print(grammar_id)
if grammar_id == "" or query_id == "" or preference_func == "":
    print("Insufficent arugments")
    sys.exit()

grammar = db["grammars"].find_one({"_id": ObjectId(grammar_id)})
if not grammar:
    print("Grammar not found")
    sys.exit()

# this be outdated
def makeGraph(mongoGraph):
    g = gt.Graph(directed=False)
    IDtoIndex = {}
    vertices = []
    # g.vertex_properties["abbrevs"] = g.new_vertex_property("string")
    # g.vertex_properties["terminality"] = g.new_vertex_property("bool")
    # g.vertex_properties["labels"] = g.new_vertex_property("string")
    # g.vertex_properties["marks"] = g.new_vertex_property("string")
    g.vertex_properties["vertex_property"] = g.new_vertex_property("python::object")
    for i, node in enumerate(mongoGraph["nodes"]):
        v = g.add_vertex()
        IDtoIndex[node["id"]] = i
        abbrevs = node["abbrev"]
        labels = node["label"]
        terminality = False
        marks = node["label"]

        vertices.append(v)

    for edge in mongoGraph["edges"]:
        u = vertices[IDtoIndex[edge["from"]]]
        v = vertices[IDtoIndex[edge["to"]]]
        g.add_edge(u, v)
    return g


def makeGrammar(grammar):
    axiom = makeGraph(grammar["axiom"])
    # gt.graph_draw(axiom, vertex_text=abbrevs, output="img/axiom.png")

    rules = []
    for i, rule in enumerate(grammar["rules"]):
        rule_name = rule["name"]
        rule_lhs = rule["lhs"]
        lhs_graph = makeGraph(rule_lhs)
        lhs = LHS(lhs_graph)
        # gt.graph_draw(lhs_graph, vertex_text=lhs_abbrevs, output="lhs"+ str(i) +".png")
        rule_rhss = rule["rhs"]
        rhss = []
        for j, rule_rhs in enumerate(rule_rhss):
            rhs_graph = makeGraph(rule_rhs["graph"])
            rhs = RHS(rhs_graph, rule_rhs["probability"])
            rhss.append(rhs)
            # gt.graph_draw(
            #     rhs_graph, vertex_text=rhs_abbrevs, output="rhs-"  + str(i)+ "-" + str(j) + ".png"
            # )
        rules.append(Rule(rule_name, lhs, rhss, 1))

        return (axiom, rules)


def makeSample(grammar_tuple, sampling_method, sample_num):

    (axiom, rules) = grammar_tuple

    gen = Generator()

    # max applications should be a range that the user specifies

    config = {
        "max_applications": random.randrange(10) + 1,
        "sampling_method": sampling_method,
    }

    gt.graph_draw(
        axiom,
        vertex_text=axiom.vp.abbrevs,
        output="imgs/axiom-" + str(sample_num) + ".png",
    )
    new_graph = axiom.copy()
    gen.applyRules(new_graph, rules, config)
    gt.graph_draw(
        new_graph,
        vertex_text=new_graph.vp.abbrevs,
        output="imgs/end-" + str(sample_num) + ".png",
    )

    # graph = {"nodes": [], "edges": []}
    # nx_G = nx.Graph()

    # id_map = {}
    # for v in axiom.vertices():
    #     v_id = uuid.uuid4()
    #     id_map[v] = str(v_id)
    #     node = {"label": labels[v], "abbrev": abbrevs[v], "id": str(v_id)}
    #     graph["nodes"].append(node)
    #     nx_G.add_node(int(v))

    # for e in axiom.edges():
    #     u, v = e
    #     edge = {"from": id_map[u], "to": id_map[v]}
    #     graph["edges"].append(edge)
    #     nx_G.add_edge(int(u), int(v), weight=1 )

    # # fe_graph = FeatherGraph()
    # fe_graph.fit([nx_G])
    # features = fe_graph.get_embedding()
    # features = []
    # for word in embed.wv.vocab:
    #     features.append(embed.wv[word])

    # features = np.array(features).flatten()

    # gpr = GPR(grammar_id,preference_func)

    # s  = 0
    # path_to_model = "models/"+grammar_id+"-"+preference_func+".pth"
    # gpr.trainGPR(features, [s])
    # score, uncertainty = gpr.predict(features)
    # print(graph)
    # sample = {"grammar_id": str(grammar["_id"]), "graph": graph, "scores": {preference_func: {"predicted_score": score,"score": 0, "uncertainty": uncertainty}}, "query_id": query_id, "scored":False}

    # print(sample)
    # response = requests.post("http://localhost:8000/samples/add", json = sample)
    # print(response.text)

    # print("done")
    # delta = datetime.now() - startTime
    # print(delta.total_seconds())


grammar_tuple = makeGrammar(grammar)
for i in range(sample_size):
    print("Sample number:", i)
    makeSample(grammar_tuple, sampling_method, i)
