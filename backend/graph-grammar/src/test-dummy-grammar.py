#!/usr/bin/env python3


import graph_tool.all as gt
from Rule import Rule, RHS, LHS
import logging
import sys
from Generator import Generator
from VertexProperty import VertexProperty
import pickle


def initilizeGraph():
    g = gt.Graph()
    # g = gt.Graph()
    g.vertex_properties["vertex_property"] = g.new_vertex_property("python::object")
    g.vertex_properties["abbrevs"] = g.new_vertex_property("string")
    return g


axiom = initilizeGraph()

v = axiom.add_vertex()
abbrev = "a"
label = "a"
terminality = False
mark = "a"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "B"
label = "B"
terminality = False
mark = "B"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "a"
label = "a"
terminality = False
mark = "a"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "A"
label = "A"
terminality = False
mark = "A"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "b"
label = "b"
terminality = False
mark = "b"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


axiom.add_edge(0, 1)
axiom.add_edge(2, 1)
axiom.add_edge(1, 3)
axiom.add_edge(3, 4)

for v in axiom.vertices():
    axiom.vp.abbrevs[v] = axiom.vp.vertex_property[v].abbrev

gt.graph_draw(axiom, vertex_text=axiom.vp.abbrevs, output="test-dormans-axiom.png")

lhs = initilizeGraph()

v = lhs.add_vertex()
abbrev = "B"
label = "B"
terminality = False
mark = "2"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = lhs.add_vertex()
abbrev = "A"
label = "A"
terminality = False
mark = "1"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

lhs.add_edge(0, 1)

for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev

gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="test-dormans-lhs.png")

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "A"
label = "A"
terminality = False
mark = "2"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "a"
label = "a"
terminality = False
mark = "1"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = rhs.add_vertex()
abbrev = "b"
label = "b"
terminality = False
mark = "3"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = rhs.add_vertex()
abbrev = "B"
label = "B"
terminality = False
mark = "4"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 3)
rhs.add_edge(1, 2)
rhs.add_edge(2, 3)

for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="test-dormans-rhs.png")

rule_lhs = LHS(lhs)
rule_rhs = RHS(rhs, 1)

rules = []

rules.append(Rule("first", rule_lhs, [rule_rhs], 1))

config = {"max_applications": 1, "sampling_method": "uniform"}
gen = Generator(axiom, rules, "dormans-grammar")

sample, sample_id = gen.applyRules(config)
sample.vertex_properties["abbrevs"] = sample.new_vertex_property("string")
for v in sample.vertices():
    sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev

gt.graph_draw(
    sample, vertex_text=sample.vp.abbrevs, output="testing-simple-dormans.png"
)
