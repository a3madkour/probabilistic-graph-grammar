#!/usr/bin/env python3


import graph_tool.all as gt
from Rule import Rule, RHS, LHS
import logging
import sys
from Generator import Generator
from VertexProperty import VertexProperty
import pickle


def initilizeGraph():
    g = gt.Graph()
    # g = gt.Graph()
    g.vertex_properties["vertex_property"] = g.new_vertex_property("python::object")
    return g


axiom = initilizeGraph()

v = axiom.add_vertex()
abbrev = "S"
label = "Start"
terminality = False
mark = "S"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "E"
label = "Enemy"
terminality = False
mark = "E"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "K"
label = "Key"
terminality = False
mark = "K"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "L"
label = "Lock"
terminality = False
mark = "L"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

# v = axiom.add_vertex()
# abbrev[v] = "E"
# label = "Enemy"
# terminality = False
# mark = "E"

v = axiom.add_vertex()
abbrev = "K"
label = "Key"
terminality = False
mark = "K"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = axiom.add_vertex()
abbrev = "P"
label = "Puzzle"
terminality = False
mark = "P"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "L"
label = "Lock"
terminality = False
mark = "L"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = axiom.add_vertex()
abbrev = "E"
label = "Enemy"
terminality = False
mark = "E"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = axiom.add_vertex()
abbrev = "T"
label = "Triforce"
terminality = False
mark = "T"
axiom.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


axiom.add_edge(0, 1)
axiom.add_edge(1, 2)
axiom.add_edge(2, 3)
axiom.add_edge(3, 4)
axiom.add_edge(4, 5)
axiom.add_edge(5, 6)
axiom.add_edge(6, 7)
axiom.add_edge(7, 8)
# axiom.add_edge(8,9)

axiom.vertex_properties["abbrevs"] = axiom.new_vertex_property("string")
for v in axiom.vertices():
    axiom.vp.abbrevs[v] = axiom.vp.vertex_property[v].abbrev
gt.graph_draw(axiom, vertex_text=axiom.vp.abbrevs, output="../data/imgs/axiom.png")

rules = []
# rule 1

lhs = initilizeGraph()

v = lhs.add_vertex()
abbrev = "S"
label = "Start"
terminality = False
mark = "S"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = lhs.add_vertex()
abbrev = "E"
label = "Enemy"
mark = "E"
terminality = False
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

lhs.add_edge(0, 1)

lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule1-lhs.png")

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "s"
label = "start"
terminality = True
mark = "S"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "SL"
label = "SoftLock"
terminality = False
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "E"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)
rhs.add_edge(0, 2)
rhs.add_edge(0, 3)
rhs.add_edge(0, 4)
rhs.add_edge(1, 4)

rule_lhs = LHS(lhs)
rule_rhs = RHS(rhs, 1)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

rules.append(Rule("first", rule_lhs, [rule_rhs], 1))
gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule1-rhs.png")

# rule 2

lhs = initilizeGraph()

v = lhs.add_vertex()
abbrev = "K"
label = "Key"
terminality = False
mark = "K"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = lhs.add_vertex()
abbrev = "L"
label = "Lock"
terminality = False
mark = "L"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

lhs.add_edge(0, 1)


lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule2-lhs.png")
rule_lhs = LHS(lhs)

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "K"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "k"
label = "key"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "l"
label = "lock"
terminality = True
mark = "L"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)
rhs.add_edge(1, 2)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule2-rhs1.png")

rule_rhs1 = RHS(rhs, 0.5)

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "k"
label = "key"
terminality = True
mark = "K"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "l"
label = "lock"
terminality = True
mark = "L"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)
rhs.add_edge(0, 2)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule2-rhs2.png")

rule_rhs2 = RHS(rhs, 0.5)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

rules.append(Rule("second", rule_lhs, [rule_rhs1, rule_rhs2], 1))

# rule 3

lhs = initilizeGraph()
v = lhs.add_vertex()
abbrev = "E"
label = "Enemy"
terminality = False
mark = "E"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = lhs.add_vertex()
abbrev = "T"
label = "Triforce"
terminality = False
mark = "T"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

lhs.add_edge(0, 1)


lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
rule_lhs = LHS(lhs)
gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule3-lhs.png")

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "E"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "t"
label = "triforce"
terminality = True
mark = "T"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)
rhs.add_edge(0, 2)
rhs.add_edge(2, 3)

rule_rhs1 = RHS(rhs, 0.333)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule3-rhs1.png")

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "E"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "t"
label = "triforce"
terminality = True
mark = "T"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)
rhs.add_edge(1, 2)

rule_rhs2 = RHS(rhs, 0.333)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule3-rhs2.png")

rhs = initilizeGraph()

v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "E"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

v = rhs.add_vertex()
abbrev = "SL"
label = "SoftLock"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)
terminality = False
mark = "None"

v = rhs.add_vertex()
abbrev = "t"
label = "triforce"
terminality = True
mark = "T"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)
rhs.add_edge(0, 2)

rule_rhs3 = RHS(rhs, 0.333)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule3-rhs3.png")

rules.append(Rule("third", rule_lhs, [rule_rhs1, rule_rhs2, rule_rhs3], 1))

# rule 4
lhs = initilizeGraph()
v = lhs.add_vertex()
abbrev = "L"
label = "Lock"
terminality = False
mark = "L"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
rule_lhs = LHS(lhs)
gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule4-lhs.png")

rhs = initilizeGraph()
v = rhs.add_vertex()
abbrev = "l"
label = "lock"
terminality = True
mark = "L"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rule_rhs = RHS(rhs, 1)
rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule4-rhs.png")

rules.append(Rule("fourth", rule_lhs, [rule_rhs], 1))

# rule 5
lhs = initilizeGraph()
v = lhs.add_vertex()
abbrev = "K"
label = "Key"
terminality = False
mark = "K"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
rule_lhs = LHS(lhs)
gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule5-lhs.png")

rhs = initilizeGraph()
v = rhs.add_vertex()
abbrev = "k"
label = "key"
terminality = True
mark = "K"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = rhs.add_vertex()
abbrev = "SL"
label = "SoftLock"
mark = "None"
terminality = False
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)

rule_rhs = RHS(rhs, 1)
rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule5-rhs.png")

rules.append(Rule("fifth", rule_lhs, [rule_rhs], 1))


# rule 6
lhs = initilizeGraph()
v = lhs.add_vertex()
abbrev = "SL"
label = "SoftLock"
terminality = False
mark = "SL"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
rule_lhs = LHS(lhs)
gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule6-lhs.png")

rhs = initilizeGraph()
v = rhs.add_vertex()
abbrev = "e"
label = "enemy"
terminality = True
mark = "SL"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


v = rhs.add_vertex()
abbrev = "sl"
label = "softlock"
terminality = True
mark = "None"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rhs.add_edge(0, 1)

rule_rhs = RHS(rhs, 1)
# print("asd")
rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule6-rhs.png")

rules.append(Rule("sixth", rule_lhs, [rule_rhs], 1))


# rule 7
lhs = initilizeGraph()
v = lhs.add_vertex()
abbrev = "P"
label = "Puzzle"
terminality = False
mark = "P"
lhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)


lhs.vertex_properties["abbrevs"] = lhs.new_vertex_property("string")
for v in lhs.vertices():
    lhs.vp.abbrevs[v] = lhs.vp.vertex_property[v].abbrev
rule_lhs = LHS(lhs)
# gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule7-lhs.png")

rhs = initilizeGraph()
v = rhs.add_vertex()
abbrev = "p"
label = "puzzle"
terminality = True
mark = "P"
rhs.vp.vertex_property[v] = VertexProperty(label, 0, abbrev, terminality, mark)

rule_rhs = RHS(rhs, 1)

rhs.vertex_properties["abbrevs"] = rhs.new_vertex_property("string")
for v in rhs.vertices():
    rhs.vp.abbrevs[v] = rhs.vp.vertex_property[v].abbrev

gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule7-rhs.png")

rules.append(Rule("seventh", rule_lhs, [rule_rhs], 1))
# #rule 8
# lhs = initilizeGraph()
# v = lhs.add_vertex()
# lhs.vp.abbrevs[v] = "E"
# lhs.vp.labels[v] = "Enemy"
# lhs.vp.terminality[v] = False
# lhs.vp.marks[v] = "E"

# rule_lhs = LHS(lhs)
# gt.graph_draw(lhs, vertex_text=lhs.vp.abbrevs, output="../data/imgs/rule8-lhs.png")

# rhs = initilizeGraph()
# v = rhs.add_vertex()
# rhs.vp.abbrevs[v] = "e"
# rhs.vp.labels[v] = "enemy"
# rhs.vp.terminality[v] = True
# rhs.vp.marks[v] = "E"


# rule_rhs = RHS(rhs, 1)
# gt.graph_draw(rhs, vertex_text=rhs.vp.abbrevs, output="../data/imgs/rule8-rhs.png")

# rules.append(Rule("eighth",rule_lhs,[rule_rhs],1))


# config = {"max_applications": 20, "sampling_method": "uniform"}
config = {"max_applications": 20, "sampling_method": "grammar"}

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

gen = Generator(axiom, rules, "test-grammar-altered")
# gen = Generator(axiom, rules, "test-grammar")
# gen.learnParameters("../data/generation-trees/test-grammar/")
# gen = Generator(axiom,rules, "test-grammar-trained")
# gen.learnParameters("../data/trees/training-set/")
# f = open("test-grammar.pkl", "wb")
f = open("test-grammar-altered.pkl", "wb")
pickle.dump(gen, f)
# for i in range(1000):
#     sample = gen.applyRules(config)

sample, sample_id = gen.applyRules(config)
sample.vertex_properties["abbrevs"] = sample.new_vertex_property("string")
for v in sample.vertices():
    sample.vp.abbrevs[v] = sample.vp.vertex_property[v].abbrev

gt.graph_draw(sample, vertex_text=sample.vp.abbrevs, output="testing.png")
