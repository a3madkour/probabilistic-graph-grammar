#!/usr/bin/env python3

import graph_tool.all as gt

# from Rule import Rule, RHS, LHS
# import logging
# import pprint
import sys
from Generator import Generator
import GenTree
from os import listdir, mkdir, remove
import shutil
from os.path import isfile, join, isdir
from grakel import Graph
from grakel.kernels import ShortestPath, WeisfeilerLehman, VertexHistogram
import numpy as np
import uuid
import networkx as nx
from networkx.algorithms.graph_hashing import weisfeiler_lehman_graph_hash
from karateclub import FeatherGraph, LDP
from GPR import GPR
import torch
import re
import pickle
from copy import deepcopy
from sklearn.metrics import mean_squared_error, r2_score
from scipy.spatial import distance
import pandas as pd

# load the samples


def leniency(graph):
    total_rooms = len(graph.get_vertices())
    safe_rooms = 0
    for v in graph.vertices():
        if graph.vp.vertex_property[v].label != "Normal":
            safe_rooms += 1
    print(safe_rooms / total_rooms)


def gtToGrakelGraph(gt_graph):
    edges = []
    node_labels = {}
    for edge in gt_graph.edges():
        e = (int(edge.source()), int(edge.target()))
        edges.append(e)
    for i in gt_graph.verticies():
        node_labels[i] = gt_graph.vp.vertex_property[i].label
        # print(gt_graph.vp.labels[v])
    g = Graph(edges, node_labels=node_labels)
    return g


def gtToNxGraph(gt_graph):
    graph = {"nodes": [], "edges": []}
    nx_G = nx.Graph()

    id_map = {}
    for v in gt_graph.vertices():
        v_id = uuid.uuid4()
        id_map[v] = str(v_id)
        node = {"label": gt_graph.vp.vertex_property[v].label, "id": str(v_id)}
        graph["nodes"].append(node)
        nx_G.add_node(int(v))

    for e in gt_graph.edges():
        u, v = e
        edge = {"from": id_map[u], "to": id_map[v]}
        graph["edges"].append(edge)
        nx_G.add_edge(int(u), int(v), weight=1)

    return nx_G


def SaveTrainingSet(samples, trees_map):
    dir_path = "../data/trees/training-set/"
    if not isdir(dir_path):
        mkdir(dir_path)
    for sample_id in samples:
        tree = trees_map[sample_id]
        tree.saveTree(dir_path + sample_id + ".pkl")


def SaveTrainingSet2(samples, grammar):
    dir_path = "../data/trees/training-set/"
    if not isdir(dir_path):
        mkdir(dir_path)
    for sample_id in samples:
        # this is really dumb we are reading a file just to dump it again
        filename = "../data/generation-trees/" + grammar + "/" + sample_id + ".pkl"
        input_file = open(filename, "rb")
        tree = pickle.load(input_file)
        tree.saveTree(dir_path + sample_id + ".pkl")


def rejectionSampling(p_x, q_x, M, sample_id_index_map, iterations=1000):
    samples = []
    for i in range(iterations):
        # let q_x[i] be a sample from the actual grammar
        u = np.random.uniform(0, M * q_x[i])
        if u <= p_x[i]:
            samples.append(sample_id_index_map[i])
    return samples


def rejectionSampling2(gpr, gen, config, hash_prob, M, iterations=1000):
    samples = []
    ldp_graph = LDP()
    for i in range(iterations):
        # let q_x[i] be a sample from the actual grammar
        sample, sample_id = gen.applyRules(config)
        nx_graph = gtToNxGraph(sample)
        hash_key = weisfeiler_lehman_graph_hash(nx_graph)
        if hash_key not in hash_prob:
            continue
        prob = hash_prob[hash_key]
        ldp_graph.fit([nx_graph])
        embed = ldp_graph.get_embedding()
        score, uncertainty = gpr.predict(embed)
        u = np.random.uniform(0, M * prob)
        if u <= score:
            samples.append(sample_id)
    return samples


def loadTrees(grammar):
    directory = "../data/generation-trees/" + grammar + "/"
    onlyfiles = [
        directory + f for f in listdir(directory) if isfile(join(directory, f))
    ]
    trees_probs_map = {}
    trees_map = {}
    for filename in onlyfiles:
        input_file = open(filename, "rb")
        tree = pickle.load(input_file)
        trees_map[tree.sample_id] = tree
        trees_probs_map[tree.sample_id] = tree.prob
    return trees_map, trees_probs_map


def loadGraphs(grammar, trees_probs_map):
    directory = "../data/sample-graphs/" + grammar + "/"
    onlyfiles = [
        directory + f for f in listdir(directory) if isfile(join(directory, f))
    ]
    nx_graphs = []
    hash_count = {}
    valid_list = []
    hash_index = {}
    hashs = []
    tree_probs = []
    sample_id_index_map = {}
    for (i, filename) in enumerate(onlyfiles):
        G = gt.load_graph(filename)
        nx_graph = gtToNxGraph(G)
        x = re.findall("(\w+-\w+-\w+-\w+-\w+).gt$", filename)
        sample_id = x[0]
        tree_probs.append(trees_probs_map[sample_id])
        sample_id_index_map[i] = sample_id
        hash_key = weisfeiler_lehman_graph_hash(nx_graph)
        if hash_key not in hash_count:
            valid_list.append(i)
            # gt.graph_draw(G, vertex_text=G.vp.abbrevs, output=hash_key+".png")
            hash_index[i] = hash_key
            hash_count[hash_key] = 0
        hash_count[hash_key] = hash_count[hash_key] + 1
        nx_graphs.append(nx_graph)
        hashs.append(hash_key)

    hash_prob = {}
    # print("-----------------------------------------------")
    for key in hash_count:
        hash_prob[key] = hash_count[key] / len(onlyfiles)

    q_x = []
    for ha in hashs:
        q_x.append(hash_prob[ha])

    q_x = np.array(q_x)

    return (
        nx_graphs,
        q_x,
        tree_probs,
        valid_list,
        hash_index,
        sample_id_index_map,
        hash_prob,
        hashs,
    )


def trainGPR(embed, valid_list, limit, user_score):

    gpr = GPR(grammar, "overall")

    model_path = "../data/models/test-grammar/overall.pth"
    if isfile(model_path):
        remove(model_path)
    train_list = valid_list[:limit]
    gpr.trainGPR(embed[train_list], user_score[:limit])

    return gpr


# user_score_map = {
#     "2e91e6ce156a6c755c7f5bea81e664c5" :0.75,
#     "3a308c9baf55a29311bf19ce02744cb5": 0.65,
#     "04c8cbc1825fa28d01f93c1941f11847": 0.6,
#     "27f40da94d7443b05b7ca5439c617424": 0.6,
#     "29cac33768f51c1604d6538207fa991d": 0.8,
#     "251028217bcb8164ca9fe00fe9e83e8b": 0.55,
#     "a0a8c67f86acf93904361754654ef5ea": 0.15,
#     "bfde9e9c89bfc063510aa737aae41f7e": 0.45,
#     "d07a87263586351f54c73426dd5cf275": 0.25,
#     "de0088e8087b54b1f4cd61090d8c8c1a": 0.65
# }

# user_score_map = {
#     "2e91e6ce156a6c755c7f5bea81e664c5" :0.9,
#     "3a308c9baf55a29311bf19ce02744cb5": 0.6,
#     "04c8cbc1825fa28d01f93c1941f11847": 0.6,
#     "27f40da94d7443b05b7ca5439c617424": 0.5,
#     "29cac33768f51c1604d6538207fa991d": 0.6,
#     "251028217bcb8164ca9fe00fe9e83e8b": 0.6,
#     "a0a8c67f86acf93904361754654ef5ea": 0.5,
#     "bfde9e9c89bfc063510aa737aae41f7e": 0.6,
#     "d07a87263586351f54c73426dd5cf275": 0.6,
#     "de0088e8087b54b1f4cd61090d8c8c1a": 0.65
# }

user_score_map = {
    "2e91e6ce156a6c755c7f5bea81e664c5": 0.675,
    "3a308c9baf55a29311bf19ce02744cb5": 0.39,
    "04c8cbc1825fa28d01f93c1941f11847": 0.36,
    "27f40da94d7443b05b7ca5439c617424": 0.3,
    "29cac33768f51c1604d6538207fa991d": 0.36,
    "251028217bcb8164ca9fe00fe9e83e8b": 0.33,
    "a0a8c67f86acf93904361754654ef5ea": 0.075,
    "bfde9e9c89bfc063510aa737aae41f7e": 0.27,
    "d07a87263586351f54c73426dd5cf275": 0.15,
    "de0088e8087b54b1f4cd61090d8c8c1a": 0.39,
    "9954c89877be06fd956ace15e7f1625e": 0.39,
    "30af8d5923a09eb4f5a76e0f78841d35": 0.39,
    "db59b9d89d2c69f85d2bbfada0f8549b": 0.39,
}

# grammar = "test-grammar-trained"
js_dist_tree_probs = []
js_dist_q_xs = []
#####
sample_size = 100
num_trials = 1
for num_trial in range(num_trials):
    # Remove all samples and trees
    grammar = "test-grammar"
    if isdir("../data/generation-trees/" + grammar):
        shutil.rmtree("../data/generation-trees/" + grammar)
    if isdir("../data/sample-graphs/" + grammar):
        shutil.rmtree("../data/sample-graphs/" + grammar)
    print("trial: ", num_trial)
    # Generate samples from original grammar
    grammar_pkl_file = open(grammar + ".pkl", "rb")
    gen = pickle.load(grammar_pkl_file)
    config = {"max_applications": 20, "sampling_method": "uniform"}
    for i in range(sample_size):
        gen.applyRules(config)
    # Load the samples
    trees_map, trees_probs_map = loadTrees(grammar)
    (
        nx_graphs,
        q_x,
        tree_probs,
        valid_list,
        hash_index,
        sample_id_index_map,
        hash_prob,
        hashs,
    ) = loadGraphs(grammar, trees_probs_map)
    print(hash_prob)
    # user score
    user_score = []
    for index in valid_list:
        user_score.append(user_score_map[hash_index[index]])

    # Embed
    ldp_graph = LDP()
    ldp_graph.fit(nx_graphs)
    embed = ldp_graph.get_embedding()

    # Train GPR
    limit = 7
    gpr = trainGPR(embed, valid_list, limit, user_score)

    # #Test the accuracy of the GPR
    # print("--------------------------------------------------")
    # print(score)
    # score, uncertainty = gpr.predict(embed[valid_list])
    # score = score.numpy().flatten()

    test_list = valid_list[limit:]
    score, uncertainty = gpr.predict(embed[test_list])
    score = score.numpy().flatten()
    print("user score: ", user_score[limit:])
    mse = mean_squared_error(user_score[limit:], score)
    r2 = r2_score(user_score[limit:], score)
    print("mse: ", mse)
    print("r2: ", r2)
    print("score: ", score)

    # Measure JS between original grammar and GPR

    # score, uncertainty = gpr.predict(embed)
    # score = score.numpy().flatten()
    #
    score = []
    for ha in hashs:
        score.append(user_score_map[ha])

    score = np.array(score)

    js_dist_tree_prob_original = distance.jensenshannon(tree_probs, score)
    js_dist_q_x_original = distance.jensenshannon(q_x, score)

    print("------------------------------------")
    print("original tree prob")
    print(js_dist_tree_prob_original)
    print("original q_x")
    print(js_dist_q_x_original)
    # print("M for q_x: ",np.max(score/q_x))
    # print("M for tree_probs: ", np.max(score/tree_probs))

    # Generate training set trees from scores
    if isdir("../data/trees/training-set"):
        shutil.rmtree("../data/trees/training-set")
    M_q_x = 15.837540723619417
    # M_tree_probs = 186601952.73322028
    # M_tree_probs = 4.804804804804805
    # samples = rejectionSampling(score,tree_probs,M_tree_probs,sample_id_index_map)
    # samples = rejectionSampling(score,q_x,M_q_x,sample_id_index_map,sample_size)
    samples = rejectionSampling2(gpr, gen, config, hash_prob, M_q_x, 1000)
    # SaveTrainingSet(samples,trees_map)
    SaveTrainingSet2(samples, grammar)

    # Generate samples from altered grammar
    grammar_altered = grammar + "-altered"
    grammar_pkl_file = open(grammar_altered + ".pkl", "rb")
    gen_altered = pickle.load(grammar_pkl_file)
    gen_altered.learnParameters("../data/trees/training-set/")

    if isdir("../data/generation-trees/" + grammar + "-altered"):
        shutil.rmtree("../data/generation-trees/" + grammar + "-altered")
    if isdir("../data/sample-graphs/" + grammar + "-altered"):
        shutil.rmtree("../data/sample-graphs/" + grammar + "-altered")

    config = {"max_applications": 20, "sampling_method": "grammar"}
    for i in range(sample_size):
        gen_altered.applyRules(config)

    trees_map, trees_probs_map = loadTrees(grammar)
    (
        nx_graphs_altered,
        q_x_altered,
        tree_probs_altered,
        valid_list,
        hash_index,
        sample_id_index_map,
        hash_prob,
        hashs,
    ) = loadGraphs(grammar, trees_probs_map)
    # print(hash_prob)

    # Embed
    ldp_graph = LDP()
    ldp_graph.fit(nx_graphs_altered)
    embed_altered = ldp_graph.get_embedding()

    score = []
    for ha in hashs:
        score.append(user_score_map[ha])

    score = np.array(score)
    # score, uncertainty = gpr.predict(embed_altered)
    # score = score.numpy().flatten()

    # Measure JS between altered grammar and the same GPR
    js_dist_tree_prob_altered = distance.jensenshannon(tree_probs_altered, score)
    js_dist_q_x_altered = distance.jensenshannon(q_x_altered, score)
    print("altered tree prob")
    print(js_dist_tree_prob_altered)
    print("altered q_x")
    print(js_dist_q_x_altered)
    # Store that
    js_dist_tree_probs.append([js_dist_tree_prob_original, js_dist_tree_prob_altered])
    js_dist_q_xs.append([js_dist_q_x_original, js_dist_q_x_altered])
    #####

js_dist_tree_probs = np.array(js_dist_tree_probs)
js_dist_q_xs = np.array(js_dist_q_xs)


np.savetxt("js_dist_tree_probs-final.csv", js_dist_tree_probs, delimiter=",")
np.savetxt("js_dist_q_xs-final.csv", js_dist_q_xs, delimiter=",")

# print(len(samples))
