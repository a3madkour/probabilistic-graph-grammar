import express, { Express } from 'express';
import mongoose, { Connection } from 'mongoose';
import routes from './routes';


const app: Express = express();
const port: string = process.env.PORT || '8000';

require('dotenv').config();

app.use(express.json());

const uri = process.env.ATLAS_URI;
console.log(uri);

mongoose.connect('mongodb://localhost:27017/graph-grammar', { useNewUrlParser: true });

// if (uri) {
//     mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true });
// }

const connection: Connection = mongoose.connection;

connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
});


app.use(routes);

app.listen(port, () => {
    return console.log(`Server is listening on ${port}`);
});
