import mongoose, { Schema, Document } from 'mongoose';

export interface Letter {
    label: string,
    abbrev: string,
    terminality: boolean
}

export interface IAlphabet extends Document {
    name: string,
    letters: Array<Letter>
}


const alphabetSchema: Schema = new Schema({
    name: { type: String, required: true },
    letters: { type: Array, required: true }
});

export default mongoose.model<IAlphabet>('Alphabet', alphabetSchema);
