import mongoose, { Schema, Document } from 'mongoose';
import { IGraph } from './graph.model';
import { IRule } from './rule.model';

interface PreferenceFunction {
    name: string
}

export interface IGrammar extends Document {
    name: string,
    alphabet_id: string,
    axiom: IGraph,
    rules: Array<IRule>,
    preference_funcs: Array<PreferenceFunction>
}


const grammarSchema: Schema = new Schema({
    name: { type: String, required: true },
    alphabet_id: { type: String, required: true },
    axiom: { type: Object, required: true },
    rules: { type: Array, required: true },
    preference_funcs: { type: Array, required: false }
});

export default mongoose.model<IGrammar>('Grammar', grammarSchema);
