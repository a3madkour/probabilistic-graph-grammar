import mongoose, { Schema, Document } from 'mongoose';

export interface IGraph extends Document {
    nodes: {
        label: string,
        abbrev: string,
        terminality: boolean,
        id: number
    }[],
    edges: {
        from: string,
        to: string
    }[]
}


const graphSchema: Schema = new Schema({
    nodes: { type: Array, required: true },
    edges: { type: Array, required: true }
});

export default mongoose.model<IGraph>('Graph', graphSchema);
