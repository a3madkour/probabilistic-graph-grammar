import mongoose, { Schema, Document } from 'mongoose';
import { IGraph } from './graph.model';

export interface IRule extends Document {
    name: string,
    lhs: IGraph,
    rhs: { probability: number, graph: IGraph }[];
}


const ruleSchema: Schema = new Schema({
    name: { type: String, required: true },
    lhs: { type: Object, required: true },
    rhs: { type: Array, required: false },
});

export default mongoose.model<IRule>('Rule', ruleSchema);
