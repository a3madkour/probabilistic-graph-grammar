import mongoose, { Schema, Document } from 'mongoose';
import { IGraph } from './graph.model';
import { IGrammar } from './grammar.model';

interface Score {
    predicted_score: number,
    score: number,
    uncertainty: number
}
export interface ISample extends Document {
    graph: IGraph,
    grammar_id: IGrammar['_id'],
    scores: Record<string, Score>,
    query_id: string,
    scored: boolean
}


const sampleSchema: Schema = new Schema({
    graph: { type: Object, required: true },
    grammar_id: { type: String, required: true },
    scores: { type: Map, required: true },
    query_id: { type: String, required: true },
    scored: { type: Boolean, required: true }
});

export default mongoose.model<ISample>('Sample', sampleSchema);
