import { Request, Response, Router } from 'express';
import Alphabet, { IAlphabet, Letter } from '../models/alphabet.model';

const alphabetsRouter: Router = Router();



alphabetsRouter.get('/', (req: Request, res: Response) => {
    Alphabet.find()
        .then(alphabet => res.json(alphabet))
        .catch(err => res.status(400).json('Error: ' + err));

});

alphabetsRouter.post('/add', (req: Request, res: Response) => {

    const name: string = req.body.name;
    const letters: Array<Letter> = req.body.letters;


    const newAlphabet: IAlphabet = new Alphabet({ name, letters });

    newAlphabet.save()
        .then((resp) => res.json(resp))
        .catch(err => res.status(400).json('Error: ' + err));

});

alphabetsRouter.get('/:id', (req: Request, res: Response) => {
    Alphabet.findById(req.params.id)
        .then(alphabet => res.json(alphabet))
        .catch((err: any) => res.status(400).json('Error: ' + err));
});
alphabetsRouter.delete('/:id', (req: Request, res: Response) => {
    Alphabet.findByIdAndDelete(req.params.id)
        .then(() => res.json('Alphabet deleted.'))
        .catch((err: any) => res.status(400).json('Error: ' + err));
});

alphabetsRouter.post('/update/:id', (req: Request, res: Response) => {
    Alphabet.findById(req.params.id)
        .then(alphabet => {
            if (alphabet) {
                alphabet.name = req.body.name;
                alphabet.letters = req.body.letters;
                alphabet.save()
                    .then(() => res.json('Alphabet updated!'))
                    .catch((err: any) => res.status(400).json('Error: ' + err));
            }
        })
        .catch((err: any) => res.status(400).json('Error: ' + err));
});
export default alphabetsRouter;
