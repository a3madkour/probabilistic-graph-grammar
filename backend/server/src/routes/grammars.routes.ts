import { Request, Response, Router } from 'express';
import Grammar, { IGrammar } from '../models/grammar.model';

const grammarsRouter: Router = Router();


grammarsRouter.get('/', (req: Request, res: Response) => {
    Grammar.find()
        .then(grammar => res.json(grammar))
        .catch((err: any) => res.status(400).json('Error: ' + err));

});

grammarsRouter.post('/add', (req: Request, res: Response) => {

    const name: IGrammar['name'] = req.body.name;
    const alphabet_id: IGrammar['alphabet_id'] = req.body.alphabet_id;
    const axiom: IGrammar['axiom'] = req.body.axiom;
    const rules: IGrammar['rules'] = req.body.rules;
    const preference_funcs: IGrammar['preference_funcs'] = req.body.preference_funcs;

    const newGrammar: IGrammar = new Grammar({ name, alphabet_id, axiom, rules, preference_funcs });

    newGrammar.save()
        .then(() => res.json('Grammar added!'))
        .catch((err: any) => res.status(400).json('Error: ' + err));

});

grammarsRouter.get('/:id', (req: Request, res: Response) => {
    Grammar.findById(req.params.id)
        .then(grammar => res.json(grammar))
        .catch((err: any) => res.status(400).json('Error: ' + err));
});
grammarsRouter.delete('/:id', (req: Request, res: Response) => {
    Grammar.findByIdAndDelete(req.params.id)
        .then(() => res.json('Grammar deleted.'))
        .catch((err: any) => res.status(400).json('Error: ' + err));
});

grammarsRouter.post('/update/:id', (req: Request, res: Response) => {
    Grammar.findById(req.params.id)
        .then(grammar => {
            if (grammar) {
                grammar.name = req.body.name;
                grammar.alphabet_id = req.body.alphabet_id;
                grammar.axiom = req.body.axiom;
                grammar.rules = req.body.rules;
                grammar.preference_funcs = req.body.preference_funcs;
                grammar.save()
                    .then(() => res.json('Grammar updated!'))
                    .catch((err: any) => res.status(400).json('Error: ' + err));
            }
        })
        .catch((err: any) => res.status(400).json('Error: ' + err));
});

export default grammarsRouter;
