import { Router } from 'express';
import grammarsRouter from './grammars.routes';
import samplesRouter from './samples.routes';
import alphabetsRouter from './alphabets.routes';
import cors from 'cors';


const routes = Router();

routes.use('/grammars', cors(), grammarsRouter);
routes.use('/samples', cors(), samplesRouter);
routes.use('/alphabets', cors(), alphabetsRouter);

export default routes;
