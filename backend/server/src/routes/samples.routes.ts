import { Request, Response, Router } from 'express';
import Sample, { ISample } from '../models/sample.model';
import { spawn } from 'child_process'

const samplesRouter: Router = Router();


samplesRouter.get('/', (req: Request, res: Response) => {
    Sample.find()
        .then(samples => res.json(samples))
        .catch(err => res.status(400).json('Error: ' + err));

});

samplesRouter.post('/add', (req: Request, res: Response) => {

    const graph: ISample['graph'] = req.body.graph;
    const grammar_id: ISample['grammar_id'] = req.body.grammar_id;
    const scores: ISample['scores'] = req.body.scores;
    const query_id: ISample['query_id'] = req.body.query_id;
    const scored: ISample['scored'] = req.body.scored;

    const newNode: ISample = new Sample({ graph, grammar_id, scores, query_id, scored });

    newNode.save()
        .then(() => res.json('Sample added!'))
        .catch(err => res.status(400).json('Error: ' + err));

});

samplesRouter.post('/update/:id', (req: Request, res: Response) => {
    //send request to update GPR with new data
    Sample.findById(req.params.id)
        .then(sample => {
            if (sample) {
                sample.graph = req.body.graph;
                sample.grammar_id = req.body.grammar_id;
                sample.scores = req.body.scores;
                sample.query_id = req.body.query_id;
                sample.scored = req.body.scored;
                sample.save()
                    .then(() => res.json('Sample updated!'))
                    .catch((err: any) => res.status(400).json('Error: ' + err));
            }
        })
        .catch((err: any) => res.status(400).json('Error: ' + err));
});

samplesRouter.post('/sample', (req: Request, res: Response) => {
    // figure out storing the queries
    const config = req.body.config
    const grammar_id: ISample['grammar_id'] = req.body.grammar_id;
    const query_id: ISample['query_id'] = req.body.query_id;

    // this is where we run the python script and indicate that it has finished
    // console.log("running python script")
    // console.log(process.cwd())
    const command = 'conda'
    const python = spawn(command, ['run', '-n', 'graph-grammar-project', 'python', '../graph-grammar/read.py', query_id, grammar_id, config.sampling_method, config.sample_size, config.selected_preference])
    python.stdout.on('data', function(data) {
        console.log("The stdout")
        console.log(data.toString())
    });

    python.on('exit', (code) => {
        //python on exit has 2s delay so we need to figure out how to make it faster
        console.log("Python done")
        Sample.find({ 'grammar_id': grammar_id, 'query_id': query_id })
            .then(samples => res.json(samples))
            .catch(err => res.status(400).json('Error: ' + err));
    });

});
export default samplesRouter;
