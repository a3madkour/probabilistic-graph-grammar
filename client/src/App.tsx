import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import * as Components from './components';

function App() {
  const [state, setState] = React.useState({ route: "/" });

  const changeRoute = (newRoute: string) => {
    setState({ route: newRoute });
  }

  return (
    <Router>
      <div>
        <Components.Navbar currentRoute={state} />
        <Switch>
          <Route path="/" exact>
            <Components.GrammarsListComponent changeRouter={changeRoute} />
          </Route>
          <Route path="/grammar/:id" >
            <Components.GrammarComponent changeRouter={changeRoute} />
          </Route>
          <Route path="/samples/:id" >
            <Components.SamplesComponent changeRouter={changeRoute} />
          </Route>
          <Route path="/alphabet/:id" >
            <Components.AlphabetEdit />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
