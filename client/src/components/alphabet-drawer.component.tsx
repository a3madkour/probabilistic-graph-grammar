import React, { useEffect, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import { Button, Icon } from '@material-ui/core';
import axios from 'axios';
import { Letter } from '../util/alphabet';
/*

   TODO:
   Load in the icons
   Function to create Icon components for each letter
   <Icon>
        <img src={YourLogo} height={25} width={25}/>
    </Icon>
   Function to make IconButtons for each Icon
   Hook the onClick to a function that sets the currentSelectedNode to the button's label

*/
const drawerWidth = 150;

/*
convert the images to svgs
 */

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

interface Props {
    setSelectedLetter: (letter: Letter) => void,
    alphabet_id: string
}
export default function AlphabetDrawer(props: Props) {
    const classes = useStyles();

    const emptyLetters: Array<Letter> = []
    /* const [state, setState] = useState(''); */
    const [state, setState] = useState({ _id: '', name: '', letters: emptyLetters });

    const handleClick = (index: number) => {

        /* console.log("index");
         * console.log(index); */
        props.setSelectedLetter(state.letters[index]);
    }

    const createIconButtons = () => {
        var result = [];
        for (var i = 0; i < state.letters.length; i++) {
            const temp = i;
            result.push(
                <IconButton onClick={(e) => handleClick(temp)} >

                    <Icon style={{ width: 75, height: 75 }} >
                        <svg width="75" height="75">
                            <circle cx="37.5" cy="37.5" r="30" fill="#aeaeae" />
                            <text x="50%" y="50%" textAnchor="middle" fill="white" fontFamily="Arial" dy=".3em">
                                {state.letters[i].abbrev}
                            </text>
Sorry, your browser does not support inline SVG.
</svg>
                    </Icon>
                </IconButton>
            )
            result.push(<Divider />);
        }
        return result;
    }

    const loadAlphabet = () => {

        /* console.log(props.alphabet_id) */
        if (props.alphabet_id === "") {
            return;
        }
        axios.get('http://localhost:8000/alphabets/' + props.alphabet_id)
            .then(response => {
                setState(response.data)
                /* console.log(response.data); */

            })
            .catch((error) => {
                console.log(error);
            })
    }

    useEffect(() => {
        loadAlphabet();
    }, [props.alphabet_id]);

    const letters = createIconButtons();
    return (
        <div className={classes.root}>
            <CssBaseline />
            <Drawer
                variant="persistent"
                anchor="right"
                open={true}
            >
                {letters}
            </Drawer>
        </div>
    );
}
