import { Button, Checkbox, Divider, Fab, FormControlLabel, Grid, TextField } from '@material-ui/core';
import useTheme from '@material-ui/core/styles/useTheme';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { Letter } from '../util/alphabet';

interface ParamTypes {
    id: string
}

export default function AlphabetEdit() {
    const emptyLetters: Array<Letter> = []
    const [state, setState] = useState({ _id: '', name: '', letters: emptyLetters });
    const theme = useTheme();
    const params: ParamTypes = useParams()
    const updateAlphabet = () => {
        axios.post('http://localhost:8000/alphabets/update/' + params.id, state)
            .then(res => console.log(res.data));
        return;
    }
    const handleLetterChange = (e: any, index: number, typ: string) => {
        var letters = state.letters;
        if (typ === 'label') {
            letters[index].label = e.currentTarget.value
        } else if (typ === "abbrev") {
            letters[index].abbrev = e.currentTarget.value
        }
        setState({ _id: state._id, name: state.name, letters: letters });
        updateAlphabet();
    }

    const handleDeleteLetter = (e: any, index: number) => {
        var letters = state.letters;
        letters.splice(index, 1);
        setState({ _id: state._id, name: state.name, letters: letters });
        updateAlphabet();
    }

    const handleCheckChange = (e: any, index: number) => {
        var letters = state.letters;
        letters[index].terminality = !state.letters[index].terminality
        setState({ _id: state._id, name: state.name, letters: letters });
        updateAlphabet();
    }
    const createLetters = () => {
        const returnValue = [];
        for (var i = 0; i < state.letters.length; i++) {
            const temp = i;

            returnValue.push(
                <form noValidate autoComplete="off" key={"state-letters-" + i.toString()} style={{ textAlign: "center" }}>
                    <TextField id={i.toString() + "-label"} value={state.letters[i].label} onChange={(e) => handleLetterChange(e, temp, "label")} label="label" key={"state-letters-" + i.toString() + "label"} />

                    <TextField id={i.toString() + "-abbrev"} value={state.letters[i].abbrev} onChange={(e) => handleLetterChange(e, temp, "abbrev")} label="abbrev" key={"state-letters-" + i.toString() + "abbreviation"} />
                    <svg width="75" height="75">
                        <circle cx="37.5" cy="37.5" r="30" fill="#aeaeae" />
                        <text x="50%" y="50%" textAnchor="middle" fill="white" fontFamily="Arial" dy=".3em">{state.letters[i].abbrev}</text>
Sorry, your browser does not support inline SVG.
</svg>
                    <Button
                        id={i.toString()}
                        variant="contained"
                        color="secondary"
                        style={
                            {
                                bottom: theme.spacing(4),
                            }}
                        startIcon={<DeleteIcon />}
                        onClick={(e) => handleDeleteLetter(e, temp)}
                    >
                        Delete
                    </Button>
                    <FormControlLabel
                        style={
                            {
                                bottom: theme.spacing(4),
                            }}
                        control={
                            <Checkbox
                                checked={state.letters[i].terminality}
                                onChange={(e) => handleCheckChange(e, temp)}
                                color="primary"
                            />
                        }
                        label="Terminal"
                    />
                </form >


            );
            if (i !== (state.letters.length - 1)) {
                returnValue.push(<Divider flexItem style={{ padding: '0.5em' }} key={"divder-" + i.toString()} />)
            }

        }
        return returnValue

    }
    const handleAddLetter = () => {
        // TODO check if the abrev is small enough
        const newLetter: Letter =
        {
            label: ' ',
            abbrev: ' ',
            terminality: false
        };

        var newLetters = state.letters;
        newLetters.push(newLetter);

        setState({ _id: state._id, name: state.name, letters: newLetters });
        updateAlphabet();
    }

    const loadAlphabet = () => {
        /* console.log(params.id) */
        axios.get('http://localhost:8000/alphabets/' + params.id)
            .then(response => {
                setState(response.data)

            })
            .catch((error) => {
                console.log(error);
            })
    }

    useEffect(() => {
        loadAlphabet();
    }, []);
    return (
        <div>
            <Grid container justify='center' alignItems="center" spacing={2} >
                <Grid item>
                    {createLetters()}
                </Grid>
            </Grid>
            <Fab
                color="primary"
                style={{
                    position: 'fixed',
                    bottom: theme.spacing(2),
                    right: theme.spacing(2)
                }}
                onClick={handleAddLetter}

            >
                <AddIcon />
            </Fab>
        </div>
    )
}
