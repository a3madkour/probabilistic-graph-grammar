import { Button, Divider, Fab, Grid, TextField } from '@material-ui/core';
import useTheme from '@material-ui/core/styles/useTheme';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AddIcon from '@material-ui/icons/Add';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { IAlphabet, Letter } from '../util/alphabet';
import EditIcon from '@material-ui/icons/Edit';
import { IGraph } from '../util/graph';
import { GraphComponent } from '.';

interface Props {
    changeRouter: (newRouter: string) => void,
    alphabet_id: string
}
export default function AlphabetView(props: Props) {


    const emptyLetters: Array<Letter> = []
    const [state, setState] = useState({ _id: '', name: '', letters: emptyLetters });
    const theme = useTheme();
    const history = useHistory();
    const location = useLocation();
    const alphabetPath = '/alphabet/' + props.alphabet_id;


    const loadAlphabet = () => {

        if (props.alphabet_id === "") {
            return;
        }
        axios.get('http://localhost:8000/alphabets/' + props.alphabet_id)
            .then(response => {
                setState(response.data)

                /* console.log(response.data); */

            })
            .catch((error) => {
                console.log(error);
            })
    }


    useEffect(() => {
        props.changeRouter(location.pathname);
        loadAlphabet();
    }, [props.alphabet_id]);

    const createLetterIcons = () => {
        const returnValue = [];

        for (var i = 0; i < state.letters.length; i++) {

            returnValue.push(
                <div>
                    <svg width="75" height="75">
                        <circle cx="37.5" cy="37.5" r="30" fill="#aeaeae" />
                        <text x="50%" y="50%" textAnchor="middle" fill="white" fontFamily="Arial" dy=".3em">
                            {state.letters[i].abbrev}
                        </text>
Sorry, your browser does not support inline SVG.
</svg>
                    <div style={{ textAlign: "center" }}>
                        {state.letters[i].label}
                    </div>
                </div >
            );

        }
        return returnValue

    }
    const emptyGraph: IGraph =
    {
        nodes: [],
        edges: []
    }

    const editAlphabet = (e: any) => {
        history.push(alphabetPath);
    }

    return (
        <div style={{ height: "90%" }}>
            <div className="alphabetView">
                {createLetterIcons()}
            </div>
            <Button
                variant="contained"
                color="default"
                startIcon={<EditIcon />}
                onClick={editAlphabet}
                style={{ height: "10%", float: "right" }}
            >
                Edit Alphabet
                                </Button>
        </div>
    )
}
