import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import axios from 'axios';
import GraphComponent from './graph.component'
import AlphabetDrawer from './alphabet-drawer.component'
import { Button, useTheme } from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save';
import { IGraph } from '../util/graph';
import { Letter } from '../util/alphabet';

interface Props {
    changeRouter: (newRouter: string) => void,
    graph: IGraph,
    alphabet_id: string,
    updateParent?: () => void
}


export default function AxiomComponent(props: Props) {

    const emptyLetter = {
        label: "",
        abbrev: "",
        terminality: false
    }
    const [state, setState] = useState({ selectedLetter: emptyLetter });
    const location = useLocation();
    const history = useHistory();
    const theme = useTheme();




    const setSelectedLetter = (letter: Letter) => {
        setState({ selectedLetter: letter });

    }

    const handleSave = (e: any) => {
        //actually save the axiom
        history.goBack();

    }
    useEffect(() => {
        props.changeRouter(location.pathname);
    }, []);

    return (
        <div className='container'>
            <AlphabetDrawer alphabet_id={props.alphabet_id} setSelectedLetter={setSelectedLetter} />
            <div className='content' >
                <GraphComponent graph={props.graph} updateParent={props.updateParent} letter={state.selectedLetter} manipulate={true} />
                <Button
                    variant="contained"
                    color="primary"
                    startIcon={<SaveIcon />}
                    onClick={handleSave}
                    style={{
                        position: 'fixed',
                        bottom: theme.spacing(4),
                        right: theme.spacing(11)
                    }}
                >
                    Save Axiom
                                </Button>
            </div>
        </div>
    )

}
