import React, { useEffect, useState } from 'react';
import { Route, Switch, useHistory, useLocation, useParams, useRouteMatch } from 'react-router-dom';
import axios from 'axios';
import GraphComponent from './graph.component'
import { Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import AxiomComponent from './axiom.component';
import RuleComponent from './rule.component';
import RulesListComponent from './rules-list.component';
import { IGraph, IRule } from '../util/graph';
import AlphabetEdit from './alphabet-edit.component';
import AlphabetView from './alphabet-view-component';

interface IGrammar {

    _id?: string,
    name: string,
    alphabet_id: string,
    axiom: IGraph,
    rules: Array<IRule>,
    preference_funcs?: [];
}

interface ParamTypes {
    id: string
}

interface Props {
    changeRouter: (newRouter: string) => void
}

interface State {
    selectedRule: IRule,
    grammar: IGrammar
}

export default function GrammarComponent(props: Props) {

    const defaultGrammar: IGrammar = {
        name: '',
        alphabet_id: '',
        axiom:
        {
            nodes: [],
            edges: []
        },
        rules: []
    }

    const emptyLetter = {
        label: "",
        abbrev: "",
        terminality: false
    }
    const emptyGraph: IGraph =
    {
        nodes: [],
        edges: []
    }

    const defaultRule: IRule = {
        name: "",
        lhs: emptyGraph,
        rhs: []
    }
    const [state, setState] = useState(
        {
            selectedRule: defaultRule,
            grammar: defaultGrammar
        });
    const location = useLocation()
    const history = useHistory();
    const params: ParamTypes = useParams()
    const axiomPath = '/grammar/' + params.id + '/axiom'
    const rulePath = '/grammar/' + params.id + '/rule/:ruleid'
    const { path, url } = useRouteMatch();

    const loadGrammar = () => {
        axios.get('http://localhost:8000/grammars/' + params.id)
            .then(response => {
                var grammar = response.data;
                setState(
                    {
                        selectedRule: state.selectedRule,
                        grammar: grammar
                    }
                );
                /* console.log(grammar); */
            })
            .catch((error) => {
                console.log(error);
            })
    }


    const editGraph = (e: any) => {

        setState({
            selectedRule: state.selectedRule,
            grammar: state.grammar
        })
        history.push(axiomPath);
    }

    const changeCurrentRule = (rule: IRule) => {
        setState({
            selectedRule: rule,
            grammar: state.grammar
        })
    }

    const updateGrammar = () => {

        setState({
            selectedRule: state.selectedRule,
            grammar: state.grammar
        });
        axios.post('http://localhost:8000/grammars/update/' + params.id, state.grammar)
            .then(res => console.log(res.data));

        return;
    }

    useEffect(() => {


        props.changeRouter(location.pathname);
        loadGrammar();

    }, []);


    return (
        <div >
            <Switch>
                <Route exact path={axiomPath} >
                    <AxiomComponent graph={state.grammar.axiom} updateParent={updateGrammar} alphabet_id={state.grammar.alphabet_id} changeRouter={props.changeRouter} />
                </Route>
                <Route exact path={rulePath} >
                    <RuleComponent rule={state.selectedRule} updateParent={updateGrammar} alphabet_id={state.grammar.alphabet_id} changeRouter={props.changeRouter} />
                </Route>
                <Route exact path={path}>
                    <div>
                        <div className="grammarContainer">
                            <div className="grammarAxiom">
                                <GraphComponent graph={state.grammar.axiom} letter={emptyLetter} manipulate={false} />
                                <Button
                                    variant="contained"
                                    color="default"
                                    startIcon={<EditIcon />}
                                    onClick={editGraph}
                                    style={{ height: "10%", float: "right" }}
                                >
                                    Edit Axiom
                                </Button>
                            </div>
                            <div className="grammarAlphabet">
                                <AlphabetView alphabet_id={state.grammar.alphabet_id} changeRouter={props.changeRouter} />
                            </div>
                        </div>
                        <div>
                            <RulesListComponent rules={state.grammar.rules} updateParent={updateGrammar} changeRouter={props.changeRouter} changeSelectRule={changeCurrentRule} />
                        </div>
                    </div>
                </Route>
            </Switch>
        </div >
    );


}
