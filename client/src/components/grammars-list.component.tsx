import React, { useEffect } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import axios from 'axios';
import { useState } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Fab, MenuItem, Paper, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, useTheme } from '@material-ui/core';
import { Table } from '@material-ui/core';
import { TablePagination } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import { Select } from '@material-ui/core';
import { IAlphabet } from '../util/alphabet';
/*
   look at the row Click prop from the data grid

*/

interface Grammar {
    name: string,
    _id: string
}

interface GrammarProps {
    deleteGrammar: (id: string) => void
    grammar: Grammar
    name: string
}

interface GrammarsListProps {
    changeRouter: (newRouter: string) => void
}


const Grammar: React.FC<GrammarProps> = props => (
    <TableRow>
        <TableCell
            key={props.grammar._id + "-name"}
            align={"left"}
            style={{ minWidth: 170 }}
        >
            {props.name}
        </TableCell>
        <TableCell
            key={props.grammar._id + "-use"}
            align={"right"}
            style={{ minWidth: 170 }}
        >
            <Link to={"/samples/" + props.grammar._id}>
                <Button
                    variant="contained"
                    startIcon={<BubbleChartIcon />}
                >
                    Use
      </Button>
            </Link>
        </TableCell>
        <TableCell
            key={props.grammar._id + "-edit"}
            align={"right"}
            style={{ minWidth: 170 }}
        >
            <Link to={"/grammar/" + props.grammar._id}>
                <Button
                    variant="contained"
                    startIcon={<EditIcon />}
                >
                    Edit
      </Button>
            </Link>
        </TableCell>
        <TableCell
            key={props.grammar._id + "-delete"}
            align={"right"}
            style={{ minWidth: 170 }}
        >
            <Button
                variant="contained"
                color="secondary"
                startIcon={<DeleteIcon />}
                onClick={() => { props.deleteGrammar(props.grammar._id) }}
            >
                Delete
      </Button>
        </TableCell>
    </TableRow>
)

export default function GrammarsListComponent(props: GrammarsListProps) {

    const defaultDialogState = {
        open: false,
        textField: "",
        error: false
    }
    const history = useHistory();

    const defaultAlphabet: IAlphabet =

    {
        _id: "",
        name: "placeholder",
        letters: []
    }

    const defaultAlphabets: Array<IAlphabet> =

        [
            defaultAlphabet
        ]



    const [state, setState] = useState({ grammars: [{ name: '', _id: '' }], dialogState: defaultDialogState, alphabets: defaultAlphabets, selectedAlphabet: defaultAlphabet });
    const location = useLocation()

    const theme = useTheme();

    const loadData = () => {

        axios.get('http://localhost:8000/grammars/')
            .then(response => {
                var grammars = response.data;
                /* console.log(response.data); */
                axios.get('http://localhost:8000/alphabets/')
                    .then(response => {
                        var alpha: IAlphabet = defaultAlphabet;
                        if (response.data.length > 0) {
                            alpha = response.data[0];
                        }
                        setState({ grammars: grammars, dialogState: defaultDialogState, alphabets: response.data, selectedAlphabet: alpha })
                        /* console.log(response.data); */
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            })
            .catch((error) => {
                console.log(error);
            })


    }

    useEffect(() => {
        props.changeRouter(location.pathname);
        loadData();
    }, []);

    const deleteGrammar = (id: string) => {
        axios.delete('http://localhost:8000/grammars/' + id)
            .then(res => console.log(res.data));

        setState({
            grammars: state.grammars.filter((el: any) => el._id !== id),
            dialogState: defaultDialogState,
            alphabets: state.alphabets,
            selectedAlphabet: state.selectedAlphabet
        })
    }

    const grammarsList = () => {
        return state.grammars.map(currentgrammar => {
            return <Grammar grammar={currentgrammar} deleteGrammar={deleteGrammar} name={currentgrammar.name} key={currentgrammar._id} />
        })
    }
    const handleChangePage = (event: unknown, newPage: number) => {
        /* setPage(newPage); */
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        /* setRowsPerPage(+event.target.value);
         * setPage(0); */
    };
    const handleClickOpen = () => {
        setState({
            grammars: state.grammars, dialogState: {
                open: true,
                textField: '',
                error: false
            },
            alphabets: state.alphabets,
            selectedAlphabet: state.selectedAlphabet
        })
    };

    const createGrammar = (name: string) => {
        // Making a new alphabet instead of selecting an existing alphabet

        /* console.log(defaultAlphabet); */
        axios.post('http://localhost:8000/alphabets/add', defaultAlphabet)
            .then(res => {

                axios.post('http://localhost:8000/grammars/add',
                    {
                        name: name,
                        alphabet_id: res.data._id,
                        axiom: {
                            nodes: [],
                            edges: []
                        },
                        rules: []
                    }
                )
                    .then(res2 => loadData());
            });

    }
    const handleSubmit = () => {
        if (state.dialogState.textField !== '') {
            createGrammar(state.dialogState.textField);
        } else {
            setState({
                grammars: state.grammars, dialogState: {
                    open: true,
                    textField: state.dialogState.textField,
                    error: true
                },
                alphabets: state.alphabets,
                selectedAlphabet: state.selectedAlphabet
            });
        }

    };

    const handleClose = () => {
        setState({ grammars: state.grammars, dialogState: defaultDialogState, alphabets: state.alphabets, selectedAlphabet: state.selectedAlphabet });
    };

    const handleChange = (e: any) => {

        setState({
            grammars: state.grammars, dialogState: {
                open: true,
                textField: e.target.value,
                error: false
            },
            alphabets: state.alphabets,
            selectedAlphabet: state.selectedAlphabet
        })
    }

    const handleSelectChange = (e: any) => {
        const alphabet = e.target.value
        /* console.log(e.target.value); */
    };

    const handleAddAlphabet = (e: any) => {
    };

    const makeMenuItems = () => {
        return state.alphabets.map(alphabet => {
            return <MenuItem key={alphabet.name} value={alphabet.name}>{alphabet.name}</MenuItem>
        })
    }

    return (
        <div >
            <h3>Grammars</h3>
            <Paper >
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    key={"name"}
                                    align={"left"}
                                    style={{ minWidth: 170 }}
                                >
                                    {"Grammar Name"}
                                </TableCell>
                                <TableCell
                                    key={"use"}
                                    align={"right"}
                                    style={{ minWidth: 170 }}
                                >
                                    {"Use Grammar"}
                                </TableCell>
                                <TableCell
                                    key={"edit"}
                                    align={"right"}
                                    style={{ minWidth: 170 }}
                                >
                                    {"Edit Grammar"}
                                </TableCell>
                                <TableCell
                                    key={"delete"}
                                    align={"right"}
                                    style={{ minWidth: 170 }}
                                >
                                    {"Delete Grammar"}
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {grammarsList()}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={2}
                    rowsPerPage={10}
                    page={0}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>

            <Fab
                color="primary"
                style={{
                    position: 'fixed',
                    bottom: theme.spacing(2),
                    right: theme.spacing(2)
                }}
                onClick={handleClickOpen}

            >
                <AddIcon />
            </Fab>
            <Dialog open={state.dialogState.open} onClose={handleClose}>
                <DialogTitle>New Grammar</DialogTitle>
                <DialogContent>
                    {/* <DialogContentText>
                        Select the alphabet to use for this grammar
                    </DialogContentText>
                    <div>
                        <Select

                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={state.selectedAlphabet.name}
                            onChange={handleSelectChange}
                            style={{ width: '100%' }}
                        >
                            {makeMenuItems()}
                        </Select>
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<AddIcon />}
                            onClick={handleAddAlphabet}
                            className='addAlphabet'
                        >
                            Add Alphabet
                                </Button>
                    </div> */}
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        onChange={handleChange}
                        value={state.dialogState.textField}
                        label="Grammar Name"
                        error={state.dialogState.error}
                        required
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Create
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    )
}
