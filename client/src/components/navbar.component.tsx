import { Typography } from '@material-ui/core';
import { Breadcrumbs } from '@material-ui/core';
import axios from 'axios';
import React, { createRef, Component, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useLocation } from 'react-router-dom';


interface State {
    route: string
    name: string
}

interface routeObject {
    route: string
}
interface Props {
    currentRoute: routeObject
}


interface ParamTypes {
    id: string
}

/*Fix this later but rather than pulling the info from database use context or something to have an app-wide state */
export default function Navbar(props: Props) {
    const location = useLocation();
    const [state, setState] = useState({ route: '', name: '' });

    const params: ParamTypes = useParams()
    const getGrammarNamebyID = (id: string) => {
        var grammar = { name: '' }
        axios.get('http://localhost:8000/grammars/' + id)
            .then(response => {
                grammar = response.data;
                setState({ route: state.route, name: grammar.name })
            })
            .catch((error) => {
                console.log(error);
            })



    }

    useEffect(() => {
        /* console.log("location changed: "); */
        /* console.log(location.pathname); */
        //updatee the nav bar with the new location pathname
        setState({ route: location.pathname, name: state.name })
        /* console.log("state changed") */
    }, []);
    const renderNavBar = () => {
        const path = props.currentRoute.route.split('/');
        /* const path = state.route.split('/'); */
        /* console.log(path); */
        var navbar = []
        switch (path.length) {
            case 2:
                return 'Home'
                break;
            case 3:
                if (path[1] == 'grammar') {
                    getGrammarNamebyID(path[2]);
                }
                navbar.push(<Link key={'grammars-list'} color="inherit" to={'/'}> Grammars</Link>)
                navbar.push(<Link key={'grammar-name'} to={'/grammar/' + path[2]}>{state.name}</Link>)
                break;
            case 4:
                if (path[3] == 'rule') {
                    console.log("something went wrong, no rule id")
                } else if (path[3] == 'axiom') {

                    navbar.push(<Link key={'grammars-list'} to={'/'}> Grammars</Link>)
                    navbar.push(<Link key={'grammars-name'} to={'/grammar/' + path[2]}>{state.name}</Link>)
                    navbar.push(<Typography color="textPrimary">Axiom</Typography>)

                }
                break;
            case 5:
                if (path[3] == 'rule') {
                    navbar.push(<Link key={'grammars-list'} to={'/'}> Grammars</Link>)
                    navbar.push(<Link key={'grammars-name'} to={'/grammar/' + path[2]}>{state.name}</Link>)
                    navbar.push(<Typography color="textPrimary">{path[4]}</Typography>)

                }
                break;


        }
        return navbar;
    }
    return (

        <Breadcrumbs aria-label="breadcrumb">
            {renderNavBar()}
        </Breadcrumbs>
    )
}
