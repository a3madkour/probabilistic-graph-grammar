import React, { createRef, Component, useState, useEffect, useContext } from 'react';
import { Link, useLocation } from 'react-router-dom';
import axios from 'axios';
import { DataSet, Network } from "vis-network/standalone/esm/vis-network"
import GraphComponent from './graph.component'
import AlphabetDrawer from './alphabet-drawer.component'
import { Button, Select, Grid, MenuItem, Dialog, DialogActions, DialogContent, TextField, DialogTitle, DialogContentText, Divider, useTheme } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { IGraph, IRule } from '../util/graph';
import { Letter } from '../util/alphabet';

interface ParamTypes {
    id: string
}

interface Props {
    changeRouter: (newRouter: string) => void
    rule: IRule,
    alphabet_id: string,
    updateParent?: () => void
}

interface DialogState {
    open: boolean
    textField: string
    error: boolean
}



export default function RulesListComponent(props: Props) {

    const defaultDialogState = {
        open: false,
        textField: "",
        error: false
    }
    const emptyLetter = {
        label: "",
        abbrev: "",
        terminality: false
    }
    const [state, setState] = useState({ currentRule: '', dialogState: defaultDialogState, selectedLetter: emptyLetter });
    const location = useLocation()


    const theme = useTheme();
    const handleChange = (e: any) => {
        setState({
            currentRule: state.currentRule, dialogState: {
                open: true,
                textField: e.target.value,
                error: false
            },
            selectedLetter: state.selectedLetter
        })
    }
    const handleClose = () => {
        setState({ currentRule: state.currentRule, dialogState: defaultDialogState, selectedLetter: state.selectedLetter });
    };



    useEffect(() => {
        props.changeRouter(location.pathname);
        /* console.log("rule recieved")
         * console.log(props.rule) */
    }, []);


    const makeNewProb = () => {
        return 1;
    }

    const handleAddRHS = () => {

        const rule = props.rule;
        if (!rule) {
            return
        }
        const rhs = rule.rhs;
        const prob = makeNewProb();

        const new_rule = JSON.parse(JSON.stringify(rule.lhs));

        const newRHS = {
            probability: prob,
            graph: new_rule
        }


        rhs.push(newRHS)
        setState({
            currentRule: state.currentRule, dialogState: defaultDialogState, selectedLetter: state.selectedLetter
        })
    }


    const createLHS = () => {
        /* if (!props.rule.lhs) {
    *     console.log(props.rule.lhs)
    *     return
    * } */

        return <GraphComponent graph={props.rule.lhs} updateParent={props.updateParent} letter={state.selectedLetter} manipulate={true} />;
    }

    const handleDeleteRHS = (e: any, index: number) => {
        var rhs = props.rule.rhs;
        rhs.splice(index, 1);
        if (props.updateParent) {
            props.updateParent();
        }
    }
    const createRHS = () => {
        const rightRule = props.rule;
        if (!rightRule) {
            return
        }
        const rhs = rightRule.rhs
        const returnValue = [];
        for (var i = 0; i < rhs.length; i++) {

            const index = i;

            returnValue.push(
                <div style={{ display: "flex" }}>
                    <Grid item className="gridElement left" >
                        <GraphComponent graph={rhs[i].graph} updateParent={props.updateParent} letter={state.selectedLetter} manipulate={true} />
                    </Grid>
                    <form noValidate autoComplete="off" className="center">
                        <TextField id="standard-basic" label="probability" />
                    </form>

                    <Button
                        id={i.toString()}
                        style={
                            {
                                width: "10%",
                                height: "10%",
                                top: theme.spacing(25),
                                right: theme.spacing(13)
                            }}
                        variant="contained"
                        color="secondary"
                        startIcon={<DeleteIcon />}
                        onClick={(e) => handleDeleteRHS(e, index)}
                    >
                        Delete
      </Button>
                </div>
            );

            if (i != (rhs.length - 1)) {
                returnValue.push(<Divider flexItem />)
            }


        }
        return returnValue

    }

    const setSelectedLetter = (letter: Letter) => {
        setState({
            currentRule: state.currentRule, dialogState: defaultDialogState, selectedLetter: letter
        })

    }

    return (
        <div className='container'>
            <AlphabetDrawer alphabet_id={props.alphabet_id} setSelectedLetter={setSelectedLetter} />
            <div className="left gridElement lhs">
                {createLHS()}
            </div>
            <div className="right">
                {createRHS()}
            </div>
            <Button
                variant="contained"
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleAddRHS}
                style={{
                    position: 'fixed',
                    bottom: theme.spacing(4),
                    right: theme.spacing(13)
                }}
            >
                Add RHS
                                </Button>

        </div>
    )

}
