import { Box, Divider, Grid, Slider, Typography, withStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import GraphComponent from './graph.component';
import { IGraph } from '../util/graph';
import { ISample } from '../util/sample';
import { useState } from 'react';
import LinearProgress, { LinearProgressProps } from '@material-ui/core/LinearProgress';
import axios from 'axios';

interface Props {
  sample: ISample,
  selectedPref: string,
  updateParent?: () => void
}

interface State {
  graph: IGraph
}
const PrettoSlider = withStyles({
  root: {
    color: '#52af77',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

function LinearProgressWithLabel(props: LinearProgressProps & { value: number, label: string }) {
  return (
    <div>
      <Typography>
        {props.label}
        {`: ${Math.round(props.value) / 100}`}
      </Typography>
      <LinearProgress variant="determinate" {...props} />
    </div>
  );
}

export default function SampleComponent(props: Props) {
  const emptyGraph: IGraph =
  {
    nodes: [
      {
        label: 'Start',
        id: 'dceb748f-7d66-4194-8200-a1e33fc4cb34',
        abbrev: 'S'
      },
      {
        label: 'End',
        id: 'b0bd97f9-d157-489e-b443-4bdf89b1fd4d',
        abbrev: 'E'
      }

    ],
    edges: [
      {
        from: 'dceb748f-7d66-4194-8200-a1e33fc4cb34',
        to: 'b0bd97f9-d157-489e-b443-4bdf89b1fd4d',
        id: '20246849-4cf3-4d22-b1e5-f49577f054dc'
      }
    ]
  }
  const emptyLetter = {
    label: "",
    abbrev: "",
    terminality: false
  }
  const [state, setState] = useState({ graph: emptyGraph });
  const handleChangeScore = (v: any) => {
    props.sample.scores[props.selectedPref].score = v
    props.sample.scored = true
    if (props.updateParent) {
      props.updateParent()
    }
  }

  const saveSample = (e: any) => {
    axios.post('http://localhost:8000/samples/update/' + props.sample._id, props.sample)
      .then(res => console.log(res.data));

  }
  useEffect(() => {
    if (!props.sample.scores[props.selectedPref]) {
      props.sample.scores[props.selectedPref] = {
        predicted_score: 0,
        score: 0,
        uncertainty: 0
      }
    }
  }, [props.selectedPref])
  return (
    <div className="sample">
      <div>
        <Typography gutterBottom>Score</Typography>
        <PrettoSlider valueLabelDisplay="auto" aria-label="pretto slider" onChange={(e, val) => handleChangeScore(val)} onChangeCommitted={saveSample} value={props.sample.scores[props.selectedPref].score} />
        <LinearProgressWithLabel value={props.sample.scores[props.selectedPref].predicted_score * 100} label={'Predicted Score'} />
        <Divider />
        <LinearProgressWithLabel value={props.sample.scores[props.selectedPref].uncertainty * 100} label={'Uncertainity'} color={'secondary'} />
        <Divider />
      </div>
      <div>
        <GraphComponent graph={props.sample.graph} letter={emptyLetter} manipulate={false} />
      </div>
    </div>
  )
}
