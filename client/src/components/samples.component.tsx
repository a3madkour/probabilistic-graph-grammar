import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Fab, FormControlLabel, MenuItem, Radio, RadioGroup, Select, TextField, useTheme } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import SampleComponent from './sample.component';
import AddIcon from '@material-ui/icons/Add';
import { useLocation, useParams } from 'react-router-dom';
import { IGraph, IRule } from '../util/graph';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

interface IGrammar {

    _id?: string,
    name: string,
    alphabet_id: string,
    axiom: IGraph,
    rules: Array<IRule>,
    preference_funcs?: Array<string>
}

interface Props {

    changeRouter: (newRouter: string) => void
}


interface ParamTypes {
    id: string
}

export default function SamplesComponent(props: Props) {
    const params: ParamTypes = useParams();
    const defaultDialogState = {
        open: false,
        textField: "",
        error: false
    }
    const defaultGrammar: IGrammar = {
        name: '',
        alphabet_id: '',
        axiom:
        {
            nodes: [],
            edges: []
        },
        rules: [],
        preference_funcs: []
    }
    const emptySampleArray: Array<any> = [];

    const location = useLocation()
    const [state, setState] = useState({
        selectedPreference: " ",
        dialogState: defaultDialogState,
        grammar: defaultGrammar,
        sample_size: 10,
        samples: emptySampleArray,
        config: { sampling_method: "grammar", sample_size: 10, selected_preference: "" }
    });

    const loadGrammar = () => {
        axios.get('http://localhost:8000/grammars/' + params.id)
            .then(response => {
                var grammar = response.data;
                var selected_pref = state.selectedPreference
                if (grammar.preference_funcs) {
                    selected_pref = grammar.preference_funcs[0]
                }
                var config = state.config
                config.selected_preference = selected_pref
                setState({
                    selectedPreference: selected_pref,
                    dialogState: defaultDialogState,
                    grammar: grammar,
                    sample_size: state.sample_size,
                    samples: state.samples,
                    config: config
                });
                /* console.log(grammar); */
            })
            .catch((error) => {
                console.log(error);
            })
    }

    useEffect(() => {
        props.changeRouter(location.pathname);
        loadGrammar();
    }, []);

    const makeSamples = () => {
        var returnValue = []

        for (var i = 0; i < state.samples.length; i++) {
            if (!state.samples[i].scores[state.selectedPreference]) {
                state.samples[i].scores[state.selectedPreference] = {
                    predicted_score: 0,
                    score: 0,
                    uncertainty: 0
                }
            }
            returnValue.push(
                <Grid item>
                    <SampleComponent sample={state.samples[i]} selectedPref={state.selectedPreference} updateParent={updateSamples} />
                </Grid>
            )
        }
        return returnValue;

    }

    const handleClickOpen = () => {
        /* console.log("Adding preference function") */
        setState({
            selectedPreference: state.selectedPreference,
            dialogState: {
                open: true,
                textField: '',
                error: false
            },
            grammar: state.grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: state.config

        })

    }

    const makeMenuItems = () => {
        if (state.grammar.preference_funcs) {
            return state.grammar.preference_funcs.map(pref => {
                return <MenuItem key={pref} value={pref}>{pref}</MenuItem>
            })

        } else {
            return
        }
    }
    const handleClose = () => {
        setState({
            selectedPreference: state.selectedPreference,
            dialogState: defaultDialogState,
            grammar: state.grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: state.config
        });
    };

    const handleChange = (e: any) => {

        setState({
            selectedPreference: state.selectedPreference,
            dialogState: {
                open: true,
                textField: e.target.value,
                error: false
            },
            grammar: state.grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: state.config
        })
    }

    const addPreferenceFunction = (name: string) => {
        var grammar = state.grammar
        if (!grammar.preference_funcs) {
            grammar.preference_funcs = [];
        }
        grammar.preference_funcs.push(name);
        /* console.log(state.grammar) */
        setState({
            selectedPreference: state.selectedPreference,
            dialogState: defaultDialogState,
            grammar: grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: state.config
        })
        axios.post('http://localhost:8000/grammars/update/' + params.id, state.grammar)
            .then(res => console.log(res.data));

    }
    const updateSamples = () => {

        setState({
            selectedPreference: state.selectedPreference,
            dialogState: state.dialogState,
            grammar: state.grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: state.config
        })
        //save scores
        /* axios.post('http://localhost:8000/grammars/update/' + params.id, state.grammar)
    *     .then(res => console.log(res.data)); */

        return;
    }
    const handleSubmit = () => {
        if (state.dialogState.textField !== '') {
            /* console.log("create a preference function"); */
            addPreferenceFunction(state.dialogState.textField);
            //close the dialog
        } else {
            setState({
                selectedPreference: state.selectedPreference,
                dialogState: {
                    open: true,
                    textField: state.dialogState.textField,
                    error: true
                },
                grammar: state.grammar,
                sample_size: state.sample_size,
                samples: state.samples,
                config: state.config
            });
        }

    };

    const handleSelectChange = (e: any) => {
        /* console.log("stuff has changed") */
        setState({
            selectedPreference: e.target.value,
            dialogState: state.dialogState,
            grammar: state.grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: state.config
        })

    }
    const handleSample = (e: any) => {

        //load config from the toggle button
        //load samples, create dummy ones for now
        var query = { config: state.config, grammar_id: state.grammar._id, query_id: uuidv4() }
        axios.post('http://localhost:8000/samples/sample/', query)
            .then(res => {
                //for now make both the current and complementary pdf the same thing
                setState({


                    selectedPreference: state.selectedPreference,
                    dialogState: state.dialogState,
                    grammar: state.grammar,
                    sample_size: state.sample_size,
                    samples: res.data,
                    config: state.config
                })
            });


    }
    const sampleSizeChange = (e: any) => {
        //TODO check if the text is a number
        setState({
            selectedPreference: state.selectedPreference,
            dialogState: state.dialogState,
            grammar: state.grammar,
            sample_size: e.currentTarget.value,
            samples: state.samples,
            config: state.config
        })

    }
    const handleShowRanked = (e: any) => {

    }
    const handleRadioChange = (e: any) => {
        var config = { sampling_method: e.target.value, sample_size: state.sample_size, selected_preference: state.selectedPreference }

        setState({


            selectedPreference: state.selectedPreference,
            dialogState: state.dialogState,
            grammar: state.grammar,
            sample_size: state.sample_size,
            samples: state.samples,
            config: config
        })
        //change what gets rendered but for now just load the config

    }
    const theme = useTheme();
    const radioStyles = {
        display: "flex",
        alignItems: "center"
    };
    return (
        <div>
            <div style={radioStyles}>
                <h1>Preference Function</h1>
                <Select

                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    style={{ width: '25%', border: "2px solid grey" }}
                    value={state.selectedPreference}
                    onChange={handleSelectChange}
                >
                    {makeMenuItems()}
                </Select>
                <RadioGroup row aria-label="position" name="position" defaultValue="uniform" onChange={handleRadioChange}>
                    <FormControlLabel
                        value="grammar"
                        control={<Radio color="primary" />}
                        label="Grammar"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="uniform"
                        control={<Radio color="primary" />}
                        label="Uniform"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="learnt_pdf"
                        control={<Radio color="primary" />}
                        label="Learnt PDF"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="bo"
                        control={<Radio color="primary" />}
                        label="Bayesian Optimization"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="uncertainty"
                        control={<Radio color="primary" />}
                        label="Uncertainty"
                        labelPlacement="top"
                    />
                </RadioGroup>
                <form noValidate autoComplete="off" key={"num_samples"} style={{ textAlign: "center" }}>
                    <TextField id={"sample_size"} onChange={sampleSizeChange} value={state.sample_size} label="label" key={"sample_size"} style={{ width: "20%" }} />
                    <Button
                        variant="contained"
                        color="default"
                        onClick={handleSample}
                        style={{ height: "50%" }}
                    >
                        Sample
                                </Button>
                </form>
            </div>
            {/* <div style={{ display: "flex", justifyContent: "space-between" }}>
                <h1>Current PDF</h1>
                <h1 >Complementary PDF</h1>
            </div> */}
            <div style={{ display: "flex", height: "70vh", width: "100vw" }}>
                <div className="samplesWindow">
                    <Grid container justify='center' spacing={2}>
                        {makeSamples()}
                    </Grid>
                </div>
                {/* <div className="samplesWindow" style={{ direction: "rtl" }}>
                    <Grid container justify='center' spacing={2} style={{ direction: "ltr" }}>
                        {makeSamples()}
                    </Grid>
                </div> */}
            </div>
            <Fab
                color="primary"
                style={{
                    position: 'fixed',
                    bottom: theme.spacing(2),
                    right: theme.spacing(2)
                }}
                onClick={handleClickOpen}

            >
                <AddIcon />
            </Fab>
            <Dialog open={state.dialogState.open} onClose={handleClose}>
                <DialogTitle>New Preference Function</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        onChange={handleChange}
                        value={state.dialogState.textField}
                        label="Preference Function Name"
                        error={state.dialogState.error}
                        required
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Create
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    )
}
